//
//  UIImage+Util.m
//  Dynasty
//
//  Created by Danilo Pantani on 22/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "UIImage+Util.h"

@implementation UIImage (Util)

- (UIImage *)thumbnail {
    NSData *imageData = UIImagePNGRepresentation(self);
    CGImageSourceRef src = CGImageSourceCreateWithData((__bridge CFDataRef)imageData, NULL);
    CFDictionaryRef options = (__bridge CFDictionaryRef)
    @{(id) kCGImageSourceCreateThumbnailWithTransform      : @YES,
      (id) kCGImageSourceCreateThumbnailFromImageAlways    : @YES,
      (id) kCGImageSourceThumbnailMaxPixelSize             : @(200)};
    
    CGImageRef scaledImageRef = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
    UIImage *scaled = [UIImage imageWithCGImage:scaledImageRef];
    CGImageRelease(scaledImageRef);
    return scaled;
}

@end
