//
//  Crashlytics+Utils.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "Crashlytics+Utils.h"
#import "NSString+Utils.h"
#import "User.h"

@implementation Crashlytics (Utils)

- (void)setGlobalUser {
    if (![[User name] isNotEmpty] ||
        ![[User email] isNotEmpty]) {
        [Crashlytics reportErrorWithDescription:@"Trying to set invalid user on Crashlytics."];
    }
    [self setUserName:[User name]];
    [self setUserEmail:[User email]];
}

+ (void)reportError:(nonnull NSError *)error {
    if (error.code == 0) {
        DDLogError(@"ERROR: %@", error.domain);
    } else {
        DDLogError(@"ERROR: %@", error.localizedDescription);
    }
    [[Crashlytics sharedInstance] recordError:error];
}

+ (void)reportErrorWithDescription:(nonnull NSString *)description {
    [self reportError:[[NSError alloc] initWithDomain:description code:0 userInfo:nil]];
}

@end
