//
//  ZeplinHelper.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "ZeplinHelper.h"

@implementation UIColor (Additions)

+ (UIColor * _Nonnull)d_yellowColor {
    return [UIColor colorWithRed:254.0f / 255.0f
                           green:207.0f / 255.0f
                            blue:51.0f / 255.0f
                           alpha:1.0f];
}

+ (UIColor * _Nonnull)d_lightOrangeColor {
    return [UIColor colorWithRed:253.0f / 255.0f
                           green:189.0f / 255.0f
                            blue:57.0f / 255.0f
                           alpha:1.0f];
}

+ (UIColor * _Nonnull)d_peachColor {
    return [UIColor colorWithRed:238.0f / 255.0f
                           green:103.0f / 255.0f
                            blue:35.0f / 255.0f
                           alpha:1.0f];
}

+ (UIColor * _Nonnull)d_blueColor {
    return [UIColor colorWithRed:0.0f / 255.0f
                           green:159.0f / 255.0f
                            blue:227.0f / 255.0f
                           alpha:1.0f];
}

+ (UIColor * _Nonnull)d_redColor {
    return [UIColor colorWithRed:255.0f / 255.0f
                           green:51.0f / 255.0f
                            blue:0.0f / 255.0f
                           alpha:1.0f];
}

@end

@implementation UIFont (Additions)

+ (UIFont * _Nonnull)d_headerFon {
    return [UIFont systemFontOfSize:8.0f weight:UIFontWeightRegular];
}

@end
