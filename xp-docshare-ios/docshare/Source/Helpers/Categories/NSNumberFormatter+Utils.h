//
//  NSNumberFormatter+Utils.h
//  Neon
//
//  Created by Luciano Sugiura on 30/01/17.
//  Copyright © 2017 Neon Pagamentos SA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (Utils)

/**
 Creates a number formatter with the format: "1,234,567.89".
 */
+ (nonnull instancetype)thousandDottedFormatter;

@end
