//
//  NSString+Utils.m
//  Neon
//
//  Created by Luciano Sugiura on 19/05/17.
//  Copyright © 2017 Neon Pagamentos SA. All rights reserved.
//

#import "NSString+Utils.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Utils)

- (NSString *)urlEncoded {

    return [self stringByAddingPercentEncodingWithAllowedCharacters:
            [[NSCharacterSet characterSetWithCharactersInString:@"!*'\"();:@&=+$,/?%#[]% "] invertedSet]];
}

/**
 This method is responsible for adding mask characters properly.
 */
- (nullable NSString *)formattedStringWithMask:(nullable NSString *)aMask {

    NSString *stringToFormat = [[self componentsSeparatedByCharactersInSet:
                                 [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                componentsJoinedByString:@""];
    NSRange range;
    range.length = 1;

    NSMutableString *formattedValue = [stringToFormat mutableCopy];

    for (int i = 0; i < aMask.length ; i++) {

        range.location = i;
        NSString *mask = [aMask substringWithRange:range];

        // Checking if there's a char mask at current position of cursor
        if (mask != nil) {

            NSString *regex = @"[0-9]*";

            NSPredicate *regextest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];

            // Checking if the character at this position isn't a digit
            if (![regextest evaluateWithObject:mask]) {

                // If the character at current position is a special char this char must be appended
                // to the user entered text

                if (formattedValue.length > range.location) {
                    [formattedValue insertString:mask atIndex:range.location];
                } else {
                    return formattedValue;
                }
            }
        }
    }

    return formattedValue;
}

- (BOOL)isEmail {
    NSString *emailRegex = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,12}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isNumber {
    NSString *emailRegex = @"[0-9]+";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isCPF {
    NSCharacterSet *characterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSString *cpf = [[self componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];

    NSUInteger i, firstSum, secondSum, firstDigit, secondDigit, firstDigitCheck, secondDigitCheck;
    if (cpf == nil) return NO;

    if (cpf.length != 11) {
        return NO;
    }

    if (([cpf isEqual:@"00000000000"]) ||
        ([cpf isEqual:@"11111111111"]) ||
        ([cpf isEqual:@"22222222222"]) ||
        ([cpf isEqual:@"33333333333"]) ||
        ([cpf isEqual:@"44444444444"]) ||
        ([cpf isEqual:@"55555555555"]) ||
        ([cpf isEqual:@"66666666666"]) ||
        ([cpf isEqual:@"77777777777"]) ||
        ([cpf isEqual:@"88888888888"]) ||
        ([cpf isEqual:@"99999999999"])) {
        return NO;
    };

    firstSum = 0;
    for (i = 0; i <= 8; i++) {
        firstSum += [[cpf substringWithRange:NSMakeRange(i, 1)] intValue] * (10 - i);
    }

    if (firstSum % 11 < 2)
        firstDigit = 0;
    else
        firstDigit = 11 - (firstSum % 11);

    secondSum = 0;
    for (i = 0; i <= 9; i++) {
        secondSum = secondSum + [[cpf substringWithRange:NSMakeRange(i, 1)] intValue] * (11 - i);
    }

    if (secondSum % 11 < 2)
        secondDigit = 0;
    else
        secondDigit = 11 - (secondSum % 11);

    firstDigitCheck = [[cpf substringWithRange:NSMakeRange(9, 1)] intValue];
    secondDigitCheck = [[cpf substringWithRange:NSMakeRange(10, 1)] intValue];

    if ((firstDigit == firstDigitCheck) && (secondDigit == secondDigitCheck))
        return YES;

    return NO;
}

- (BOOL)isCNPJ {
    
    NSCharacterSet *characterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSString *cnpj = [[self componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
    
    if ([cnpj length] != 14) {
        return NO;
    }
    
    char a[14];
    int  b = 0;
    char c[13] = {6,5,4,3,2,9,8,7,6,5,4,3,2};
    int  x = 0;
    
    for (int i = 0; i < 12; i++) {
        NSString *str = [NSString stringWithFormat: @"%C", [cnpj characterAtIndex:i]];
        a[i] = [str intValue];
        b += a[i] * c[i+1];
    }
    
    x = b % 11;
    if (x < 2) {
        a[12] = 0;
    } else {
        a[12] = 11-x;
    }
    
    b = 0;
    for (int y = 0; y < 13; y++) {
        b += (a[y] * c[y]);
    }
    
    x = b % 11;
    if (x < 2) {
        a[13] = 0;
    }
    else {
        a[13] = 11-x;
    }
    
    NSString *digito1 = [NSString stringWithFormat: @"%C", [cnpj characterAtIndex:12]];
    NSString *digito2 = [NSString stringWithFormat: @"%C", [cnpj characterAtIndex:13]];
    int dgt1 = [digito1 intValue];
    int dgt2 = [digito2 intValue];
    
    if ((dgt1 != a[12]) || (dgt2 != a[13])) {
        return NO;
    }
    
    return YES;
}

- (BOOL)isMobilePhoneNumber {
    NSCharacterSet *invalidCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    BOOL hasOnlyValidCharacters = [self rangeOfCharacterFromSet:invalidCharacterSet].location == NSNotFound;
    BOOL hasCorrectSize = self.length == 9;
    BOOL isFirstDigitNine = self.length > 0 && [self characterAtIndex:0] == '9';
    return hasOnlyValidCharacters && isFirstDigitNine && hasCorrectSize;
}

- (BOOL)isFullMobilePhoneNumber {
    
    NSString *phoneNumber = [self stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSCharacterSet *invalidCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    BOOL hasOnlyValidCharacters = [phoneNumber rangeOfCharacterFromSet:invalidCharacterSet].location == NSNotFound;
    BOOL hasCorrectSize = phoneNumber.length == 11;
    BOOL isFirstDigitNine = phoneNumber.length > 2 && [phoneNumber characterAtIndex:2] == '9';
    return hasOnlyValidCharacters && isFirstDigitNine && hasCorrectSize;
}

- (BOOL)isName {
    NSMutableCharacterSet *validCharacterSet = [NSMutableCharacterSet new];
    [validCharacterSet formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
    [validCharacterSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
    [validCharacterSet formUnionWithCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
    NSCharacterSet *invalidCharacterSet = [validCharacterSet invertedSet];
    return [self rangeOfCharacterFromSet:invalidCharacterSet].location == NSNotFound && [self isNotEmpty];
}

- (BOOL)isNotEmpty {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0;
}

- (BOOL)hasNumbersOrWhiteSpacesOnly {
    NSMutableCharacterSet *validCharacterSet = [NSMutableCharacterSet new];
    [validCharacterSet formUnionWithCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
    [validCharacterSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
    NSCharacterSet *invalidCharacterSet = [validCharacterSet invertedSet];
    return [self rangeOfCharacterFromSet:invalidCharacterSet].location == NSNotFound;
}

+ (NSString *)initialsFromFullName:(NSString *)fullName {
    NSCharacterSet *characterSetToTrim = [NSCharacterSet whitespaceCharacterSet];
    NSString *normalizedName = [fullName stringByTrimmingCharactersInSet:characterSetToTrim];

    NSMutableString *initialsString = [NSMutableString new];

    NSArray<NSString *> *names = [normalizedName componentsSeparatedByString:@" "];
    if (names.count > 0) {
        NSMutableCharacterSet *validCharacterSet = [NSMutableCharacterSet new];
        [validCharacterSet formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
        [validCharacterSet formUnionWithCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
        
        NSString *firstName = names.firstObject;
        if (firstName.length > 0 && [validCharacterSet characterIsMember:[firstName characterAtIndex:0]]) {
            [initialsString appendString:[firstName substringToIndex:1].uppercaseString];
        }
        
        NSString *lastName = names.lastObject;
        if (lastName != firstName &&
            lastName.length > 0 &&
            [validCharacterSet characterIsMember:[lastName characterAtIndex:0]]) {
            
            [initialsString appendString:[lastName substringToIndex:1].uppercaseString];
            
        }
    }

    return initialsString;
}

- (BOOL)containsHTMLTags {
    NSString *pattern = @"</?\\w+((\\s+\\w+(\\s*=\\s*(?:\".*?\"|'.*?'|[\\^'\">\\s]+))?)+\\s*|\\s*)/?>";
    NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:pattern
                                                                      options:NSRegularExpressionCaseInsensitive
                                                                        error:nil];
    return [regex firstMatchInString:self options:NSMatchingReportCompletion range:NSMakeRange(0, self.length)] != nil;
}

- (nonnull NSString*)sha256 {
    const char *s = [self cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(keyData.bytes, (CC_LONG) keyData.length, digest);
    NSData *output = [NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString *hash = [output description];
    
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    return hash;
}

@end
