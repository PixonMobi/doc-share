//
//  NSString+Base64.h
//  Dynasty
//
//  Created by Danilo Pantani on 15/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Base64)

- (UIImage *)decodeBase64;

- (UIImage *)decodeBase64Avatar;

@end
