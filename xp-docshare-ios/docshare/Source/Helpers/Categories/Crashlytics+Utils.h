//
//  Crashlytics+Utils.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Crashlytics/Crashlytics.h>

@class NEClient;

@interface Crashlytics (Utils)

- (void)setGlobalUser;

/**
 Reports a non-fatal error to Crashlytics.
 In fact, Crashlytics sends all errors on next app launch.
 @param error the NSError instance to be reported.
 */
+ (void)reportError:(nonnull NSError *)error;

/**
 Reports a non-fatal error to Crashlytics with a NSError containing the given description.
 @param description a description to be contained in reported NSError.
 */
+ (void)reportErrorWithDescription:(nonnull NSString *)description;

@end
