//
//  NSDictionary+JSON.h
//  Dynasty
//
//  Created by Danilo Pantani on 15/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSON)

- (NSString*)jsonStringWithPrettyPrint:(BOOL) prettyPrint;

@end
