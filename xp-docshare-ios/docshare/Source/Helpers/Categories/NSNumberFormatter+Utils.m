//
//  NSNumberFormatter+Utils.m
//  Neon
//
//  Created by Luciano Sugiura on 30/01/17.
//  Copyright © 2017 Neon Pagamentos SA. All rights reserved.
//

#import "NSNumberFormatter+Utils.h"

@implementation NSNumberFormatter (Utils)

+ (nonnull instancetype)thousandDottedFormatter {
    static dispatch_once_t onceToken;
    static NSNumberFormatter *formatter;
    dispatch_once(&onceToken, ^{
        formatter = [[NSNumberFormatter alloc] init];
        formatter.positiveFormat = @"#,##0.00";
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    });
    return formatter;
}

@end
