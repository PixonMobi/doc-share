//
//  UIImage+Util.h
//  Dynasty
//
//  Created by Danilo Pantani on 22/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Util)

- (UIImage *)thumbnail;

@end
