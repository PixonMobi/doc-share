//
//  NSString+Utils.h
//  Neon
//
//  Created by Luciano Sugiura on 19/05/17.
//  Copyright © 2017 Neon Pagamentos SA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

- (nonnull NSString *)urlEncoded;

- (nullable NSString *)formattedStringWithMask:(nullable NSString *)aMask;

/**
 Checks if the string is a valid e-mail.
 
 @return YES if the string is an e-mail, NO otherwise.
 */
- (BOOL)isEmail;

/**
 Checks if the string is a valid CPF.
 
 @return YES if the string is a CPF, NO otherwise.
 */
- (BOOL)isCPF;


/**
 Checks if the string is a valid CNPJ.
 
 @return YES if the string is a CNPJ, NO otherwise.
 */
- (BOOL)isCNPJ;

/**
 Checks if the string is a valid full mobile phone number (which includes region code and the number itself.
 Example: 11951796933
 
 @return YES if the string is a mobile phone number, NO otherwise.
 */
- (BOOL)isFullMobilePhoneNumber;

/**
 Checks if the string is a valid mobile phone number.
 Example: 951796933
 
 @return YES if the string is a mobile phone number, NO otherwise.
 */
- (BOOL)isMobilePhoneNumber;

/**
 Checks if the string is a valid name (has only letters, white spaces and dots).
 
 @return YES if the string is a name (according to description above), NO otherwise.
 */
- (BOOL)isName;

/**
 Checks if the string is a valid number.
 
 @return YES if the string is a number, NO otherwise.
 */
- (BOOL)isNumber;

/**
 Checks if the string has at least a character that isn't a blank space or a new line.
 
 @return YES if the string is not empty (according to description above), NO otherwise.
 */
- (BOOL)isNotEmpty;

/**
 Checks if the string has only numbers.

 @return YES if the string has numbers only, NO otherwise.
 */
- (BOOL)hasNumbersOrWhiteSpacesOnly;

/**
 Returns a string based on a full name initials, limited to two characters.
 Examples:
    "Renan Benevides Cargnin" -> "RB"
    "Renan Cargnin" -> "RC"
    "Renan" -> "R"
    "" -> ""

 @param fullName The full name string from which the initials will be taken.
 @return The initials string.
 */
+ (nonnull NSString *)initialsFromFullName:(nonnull NSString *)fullName;

/**
 Detects the presence of HTML tags in strings.

 @return YES if the string contains HTML tags, NO otherwise.
 */
- (BOOL)containsHTMLTags;

/**
 Convert string into a SHA256 hash.
 
 @return the SHA256 hash from string.
 */
- (nonnull NSString*)sha256;

@end
