//
//  ZeplinHelper.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

@class UIColor;
@class UIFont;

@interface UIColor (Additions)

+ (UIColor * _Nonnull)d_yellowColor;
+ (UIColor * _Nonnull)d_lightOrangeColor;
+ (UIColor * _Nonnull)d_peachColor;
+ (UIColor * _Nonnull)d_blueColor;
+ (UIColor * _Nonnull)d_redColor;

@end

// Sample text styles

@interface UIFont (Additions)

+ (UIFont * _Nonnull)d_headerFon;

@end
