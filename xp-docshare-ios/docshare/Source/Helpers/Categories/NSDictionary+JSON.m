//
//  NSDictionary+JSON.m
//  Dynasty
//
//  Created by Danilo Pantani on 15/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "NSDictionary+JSON.h"

@implementation NSDictionary (JSON)

- (NSString *)jsonStringWithPrettyPrint:(BOOL) prettyPrint {
    NSError *error;
    NSJSONWritingOptions options = prettyPrint ? NSJSONWritingPrettyPrinted : 0;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:options
                                                         error:&error];
    
    if (!jsonData) {
        DDLogError(@"jsonStringWithPrettyPrintError: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

@end
