//
//  NSString+Base64.m
//  Dynasty
//
//  Created by Danilo Pantani on 15/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "NSString+Base64.h"

@implementation NSString (Base64)

- (UIImage *)decodeBase64 {
    NSData *data = [[NSData alloc] initWithBase64EncodedString:self options:NSDataBase64DecodingIgnoreUnknownCharacters];
//    NSData *data = [self dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    return data ? [UIImage imageWithData:data] : nil;
}

- (UIImage *)decodeBase64Avatar {
    UIImage *img = [self decodeBase64];
    return img ? img : [UIImage imageNamed:@"imgAvatar"];
}

@end
