//
//  OperationQueueManager.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 This class coordinates the operations.
 */
@interface OperationQueueManager : NSObject

/**
 Returns the global OperationQueueManager instance.
 
 @return OperationQueueManager instance.
 */
+ (instancetype)sharedInstance;

/**
 Add an operation to an operation queue.
 
 @param operation - the new operation to be added.
 */
- (void)addOperation:(NSOperation *)operation;
+ (void)addOperation:(NSOperation *)operation;

@end
