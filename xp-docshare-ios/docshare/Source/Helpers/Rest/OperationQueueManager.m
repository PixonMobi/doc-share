//
//  OperationQueueManager.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "OperationQueueManager.h"

@interface OperationQueueManager ()

/**
 NSOperationQueue that operations will be added to.
 */
@property (nonatomic, strong) NSOperationQueue *queue;

@end

@implementation OperationQueueManager

#pragma mark - SharedInstance

+ (instancetype)sharedInstance {
    static OperationQueueManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[OperationQueueManager alloc] init];
    });
    return sharedInstance;
}

+ (void)addOperation:(NSOperation *)operation {
    [[OperationQueueManager sharedInstance] addOperation:operation];
}

#pragma mark - Init

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.queue = [[NSOperationQueue alloc] init];
    }
    
    return self;
}

#pragma mark - AddOperation

- (void)addOperation:(NSOperation *)operation {
    [self.queue addOperation:operation];
}

@end
