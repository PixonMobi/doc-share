//
//  Utils.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIViewController;

//-----------------------------------------------------------------------------
#pragma mark - System Version
//-----------------------------------------------------------------------------

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface Utils : NSObject

//-----------------------------------------------------------------------------
#pragma mark - Strings
//-----------------------------------------------------------------------------

/**
 Retrives the string with specified key from appropriate strings file.
 @param key the key for desired text.
 @return the corresponding string.
 */
NSString * _Nonnull tr(NSString * _Nonnull key);

//-----------------------------------------------------------------------------
#pragma mark - JSON parsing
//-----------------------------------------------------------------------------

/**
 Checks a value type, returning the value if the given object is of given type.
 @param value the variable to be type-checked.
 @param type the class to check for.
 @return the value if it is of the given type, nil otherwise.
 */
+ (nullable id)value:(nullable id)value type:(nonnull Class)type;

/**
 Checks if the value is convertible to a NSString and returns it if it is.
 
 @param value The value to be converted to a NSString.
 @return The NSString got by converting `value`.
 */
+ (nullable NSString *)stringFromValue:(nullable id)value;

/**
 Checks if the value is convertible to a NSNumber and returns it if it is.
 
 @param value The value to be converted to a NSNumber.
 @return The NSNumber got by converting `value`.
 */
+ (nullable NSNumber *)numberFromValue:(nullable id)value;

/**
 Checks if the value is convertible to a NSInteger and returns it if it is.
 
 @param value The value to be converted to a NSInteger.
 @param defaultInteger The default value NSInteger to be returned if value is not convertible.
 @return The NSString got by converting `value` or `defaultInteger` if conversion is not possible.
 */
+ (NSInteger)integerFromValue:(nullable id)value defaultInteger:(NSInteger)defaultInteger;

/**
 Checks if the value is convertible to a BOOL and returns it if it is.
 
 @param value The value to be converted to a BOOL.
 @param defaultBoolean The default value BOOL to be returned if value is not convertible.
 @return The BOOL got by converting `value` or `defaultBoolean` if conversion is not possible.
 */
+ (BOOL)booleanFromValue:(nullable id)value defaultBoolean:(BOOL)defaultBoolean;

/**
 Checks if the value is convertible to a NSDate and returns it if it is.
 
 @param value The value to be converted to a NSDate.
 @return The NSString got by converting `value`.
 */
+ (nullable NSDate *)dateFromValue:(nullable id)value;

/**
 Convert number from string.
 
 @param string The value to be converted to a NSNumber.
 @return The NSNumber got by converting `value`.
 */
+ (nullable NSNumber *)numberFromString:(nullable NSString *)string;

//-----------------------------------------------------------------------------
#pragma mark - GCD utilities
//-----------------------------------------------------------------------------

/**
 Calls asynchronously the specified block immediately on the main queue.
 @param block the block to be executed asynchronously.
 */
+ (void)asyncMain:(nonnull void (^)(void))block;

/**
 Calls asynchronously the specified block, after @c delay seconds, on the main queue.
 @param delay the delay in seconds.
 @param block the block to be executed asynchronously.
 */
+ (void)asyncMain:(NSTimeInterval)delay block:(nonnull void (^)(void))block;

/**
 Calls asynchronously the specified block immediately, on a background queue.
 @param block the block to be executed asynchronously.
 */
+ (void)asyncBg:(nonnull void (^)(void))block;

/**
 Calls asynchronously the specified block, after @c delay seconds, on a background queue.
 @param delay the delay in seconds.
 @param block the block to be executed asynchronously.
 */
+ (void)asyncBg:(NSTimeInterval)delay block:(nonnull void (^)(void))block;

/**
 Calls a block asynchronously on a background queue, then call another block asynchronously on main queue once the
 previous block is finished.
 @param blockBg the block to be executed in a background queue.
 @param blockMain the block to be called asynchronously on the main queue, after @c blockBg is finished.
 */
+ (void)asyncBg:(nonnull void (^)(void))blockBg thenMain:(nonnull void (^)(void))blockMain;

//-----------------------------------------------------------------------------
#pragma mark - UI
//-----------------------------------------------------------------------------

/**
 Show quick alert with only ok button.
 @param message the message alert.
 @param controller the controller what alert appear.
 */
+ (void)showAlert:(nonnull NSString *)message
   fromController:(nonnull UIViewController *)controller;

/**
 Show quick alert with only ok button.
 @param title the title alert.
 @param message the message alert.
 @param controller the controller what alert appear.
 */
+ (void)showAlertWithTitle:(nonnull NSString *)title
                   message:(nonnull NSString *)message
            fromController:(nonnull UIViewController *)controller;

/**
 Show quick default error alert with only ok button.
 @param controller the controller what alert appear.
 */
+ (void)showAlertErrorFromController:(nonnull UIViewController *)controller;

/**
 Show quick alert from ApiMessage with only ok button.
 @param message the ApiMessage alert.
 @param controller the controller what alert appear.
 */
+ (void)showAlertMessage:(nonnull ApiMessage *)message
          fromController:(nonnull UIViewController *)controller;

/**
 Create QRCode from string
 @param qrString The string to generate QRCode.
 @return The QRCode Image.
 */
+ (nonnull CIImage *)createQRForString:(nonnull NSString *)qrString;

//-----------------------------------------------------------------------------
#pragma mark - Masks
//-----------------------------------------------------------------------------

+ (BOOL)returnForShouldChangeCharactersInRange:(NSRange)range;

+ (nonnull NSString *)currencyMaskWithText:(nonnull NSString *)text andRange:(NSRange)range;
    
@end
