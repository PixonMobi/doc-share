//
//  Utils.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "Utils.h"

@import UserNotifications;

@implementation Utils

//-----------------------------------------------------------------------------
#pragma mark - Strings
//-----------------------------------------------------------------------------

NSString * tr(NSString *key) {
    return NSLocalizedStringFromTable(key, @"Strings", nil);
}

//-----------------------------------------------------------------------------
#pragma mark - JSON parsing
//-----------------------------------------------------------------------------

+ (nullable NSString *)stringFromValue:(nullable id)value {
    if ([value isKindOfClass:[NSString class]]) {
        return value;
    }
    if ([value isKindOfClass:[NSNumber class]]) {
        return [value stringValue];
    }
    return nil;
}

+ (nullable NSNumber *)numberFromValue:(nullable id)value {
    if ([value isKindOfClass:[NSNumber class]]) {
        return value;
    }
    if ([value isKindOfClass:[NSString class]]) {
        return @([value floatValue]);
    }
    return nil;
}

+ (NSInteger)integerFromValue:(nullable id)value defaultInteger:(NSInteger)defaultInteger {
    if ([value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSString class]]) {
        return [value integerValue];
    }
    return defaultInteger;
}

+ (BOOL)booleanFromValue:(nullable id)value defaultBoolean:(BOOL)defaultBoolean {
    if ([value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSString class]]) {
        return [value boolValue];
    }
    return defaultBoolean;
}

+ (nullable id)value:(nullable id)value type:(nonnull Class)type {
    if ([value isKindOfClass:type]) {
        return value;
    }
    return nil;
}

+ (nullable NSDate *)dateFromValue:(nullable id)value {
    if ([value isKindOfClass:[NSString class]]) {
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
        dateFormatter.dateFormat = kJSONDateFormat;
        NSDate *date = [dateFormatter dateFromString:value];
        return date;
    }
    return nil;
}

+ (nullable NSNumber *)numberFromString:(nullable NSString *)string {
    if (!string) {
        return @0;
    }
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    f.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSNumber *number = [f numberFromString:string];
    return number ? number : @0;
}

//-----------------------------------------------------------------------------
#pragma mark - GCD utils
//-----------------------------------------------------------------------------

+ (void)asyncMain:(nonnull void (^)(void))block {
    [self asyncMain:0 block:block];
}

+ (void)asyncMain:(NSTimeInterval)delay block:(nonnull void (^)(void))block {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), block);
}

+ (void)asyncBg:(nonnull void (^)(void))block {
    [self asyncBg:0 block:block];
}

+ (void)asyncBg:(NSTimeInterval)delay block:(nonnull void (^)(void))block {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)),
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   block);
}

+ (void)asyncBg:(nonnull void (^)(void))blockBg thenMain:(nonnull void (^)(void))blockMain {
    [self asyncBg:^{
        blockBg();
        [self asyncMain:blockMain];
    }];
}

//-----------------------------------------------------------------------------
#pragma mark - Mask
//-----------------------------------------------------------------------------

+ (BOOL)returnForShouldChangeCharactersInRange:(NSRange)range {
    return range.length == 1 ? YES : NO;
}

+ (nonnull NSString *)currencyMaskWithText:(nonnull NSString *)text andRange:(NSRange)range {
    NSInteger charCount = [text length];
    NSString *charText = [text substringFromIndex:charCount-1];
    if ([charText isEqualToString:@"."] || [charText isEqualToString:@" "]) {
        return [text substringToIndex:charCount-1];
    }
    
    NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789. "];
    NSString *removeWord = [text stringByTrimmingCharactersInSet:numSet];
    
    if ([text rangeOfString:removeWord].location != NSNotFound) {
        return [text substringToIndex:charCount-1];
    }
    
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    charCount = [text length];
    
    if (range.length == 1) {
        if (charCount < 5) {
            text = [text stringByReplacingOccurrencesOfString:@"." withString:@""];
            text = [NSString stringWithFormat:@"0.%@",text];
        } else {
            text = [text stringByReplacingOccurrencesOfString:@"." withString:@""];
            text = [NSString stringWithFormat:@"%@.%@",[text substringToIndex:text.length-3],[text substringFromIndex:text.length-3]];
        }
    } else {
        if (charCount == 1) {
            text = [NSString stringWithFormat:@"0.%@",text];
        } else {
            if (charCount == 5 && [text hasPrefix:@"0"]) {
                text = [text substringFromIndex:1];
            }
            text = [text stringByReplacingOccurrencesOfString:@"." withString:@""];
            text = [NSString stringWithFormat:@"%@.%@",[text substringToIndex:text.length-2],[text substringFromIndex:text.length-2]];
        }
    }
    return text;
}

//-----------------------------------------------------------------------------
#pragma mark - Validation
//-----------------------------------------------------------------------------

+ (BOOL)validateCNPJWithString:(nonnull NSString *)cnpj {
    
    cnpj = [cnpj stringByReplacingOccurrencesOfString:@"." withString:@""];
    cnpj = [cnpj stringByReplacingOccurrencesOfString:@"/" withString:@""];
    cnpj = [cnpj stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    if ([cnpj length] != 14) {
        return NO;
    }
    
    char a[14];
    int  b = 0;
    char c[13] = {6,5,4,3,2,9,8,7,6,5,4,3,2};
    int  x = 0;
    
    for (int i = 0; i < 12; i++) {
        NSString *str = [NSString stringWithFormat: @"%C", [cnpj characterAtIndex:i]];
        a[i] = [str intValue];
        b += a[i] * c[i+1];
    }
    
    x = b % 11;
    if (x < 2) {
        a[12] = 0;
    } else {
        a[12] = 11-x;
    }
    
    b = 0;
    for (int y = 0; y < 13; y++) {
        b += (a[y] * c[y]);
    }
    
    x = b % 11;
    if (x < 2) {
        a[13] = 0;
    }
    else {
        a[13] = 11-x;
    }
    
    NSString *digito1 = [NSString stringWithFormat: @"%C", [cnpj characterAtIndex:12]];
    NSString *digito2 = [NSString stringWithFormat: @"%C", [cnpj characterAtIndex:13]];
    int dgt1 = [digito1 intValue];
    int dgt2 = [digito2 intValue];
    
    if ((dgt1 != a[12]) || (dgt2 != a[13])) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)validateCPFWithString:(nonnull NSString *)cpf {
    
    cpf = [[cpf componentsSeparatedByCharactersInSet:
            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]componentsJoinedByString:@""];
    
    NSUInteger i, firstSum, secondSum, firstDigit, secondDigit, firstDigitCheck, secondDigitCheck;
    if(cpf == nil) {
        return NO;
    }
    
    if ([cpf length] != 11||
        ([cpf isEqual:@"00000000000"]) ||
        ([cpf isEqual:@"11111111111"]) ||
        ([cpf isEqual:@"22222222222"]) ||
        ([cpf isEqual:@"33333333333"]) ||
        ([cpf isEqual:@"44444444444"]) ||
        ([cpf isEqual:@"55555555555"]) ||
        ([cpf isEqual:@"66666666666"]) ||
        ([cpf isEqual:@"77777777777"]) ||
        ([cpf isEqual:@"88888888888"]) ||
        ([cpf isEqual:@"99999999999"])) {
        return NO;
    }
    
    firstSum = 0;
    for (i = 0; i <= 8; i++) {
        firstSum += [[cpf substringWithRange:NSMakeRange(i, 1)] intValue] * (10 - i);
    }
    
    if (firstSum % 11 < 2) {
        firstDigit = 0;
    } else {
        firstDigit = 11 - (firstSum % 11);
    }
    
    secondSum = 0;
    for (i = 0; i <= 9; i++) {
        secondSum = secondSum + [[cpf substringWithRange:NSMakeRange(i, 1)] intValue] * (11 - i);
    }
    
    if (secondSum % 11 < 2) {
        secondDigit = 0;
    } else {
        secondDigit = 11 - (secondSum % 11);
    }
    
    firstDigitCheck = [[cpf substringWithRange:NSMakeRange(9, 1)] intValue];
    secondDigitCheck = [[cpf substringWithRange:NSMakeRange(10, 1)] intValue];
    
    if ((firstDigit == firstDigitCheck) && (secondDigit == secondDigitCheck)) {
        return YES;
    }
    
    return NO;
}

+ (void)showAlertErrorFromController:(nonnull UIViewController *)controller {
    ApiMessage *message = [ApiMessage defaultErrorMessage];
    [Utils showAlertMessage:message fromController:controller];
}

+ (void)showAlertMessage:(nonnull ApiMessage *)message
          fromController:(nonnull UIViewController *)controller {
    [Utils showAlertWithTitle:message.title
                      message:message.message
               fromController:controller];
}

+ (void)showAlert:(nonnull NSString *)message
   fromController:(nonnull UIViewController *)controller {
    [Utils showAlertWithTitle:@"Dynasty"
                      message:message
               fromController:controller];
}

+ (void)showAlertWithTitle:(nonnull NSString *)title
                   message:(nonnull NSString *)message
            fromController:(nonnull UIViewController *)controller {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    [alert addAction:okButton];
    [controller presentViewController:alert animated:YES completion:nil];
}

+ (nonnull CIImage *)createQRForString:(nonnull NSString *)qrString {
    NSData *stringData = [qrString dataUsingEncoding:NSISOLatin1StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(6.0f, 6.0f);
    CIImage *output = [qrFilter.outputImage imageByApplyingTransform: transform];
    
    return output;
}

@end
