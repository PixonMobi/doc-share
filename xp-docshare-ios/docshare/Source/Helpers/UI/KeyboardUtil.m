//
//  KeyboardUtil.m
//  Vimu
//
//  Created by Danilo Pantani on 22/10/16.
//  Copyright (c) 2012 Danilo Pantani. All rights reserved.
//

#import "KeyboardUtil.h"
#import "Constants.h"

@interface KeyboardUtil ()

@property (strong, nonatomic) UIView *view;
@property (assign, nonatomic) float height;

@end

@implementation KeyboardUtil

#pragma mark - LifeCycle

- (id)initFromView:(UIView *)view {
    self = [super init];
    if (self) {
        self.view = view;
        self.height = view.frame.size.height;
    }
    return self;
}

#pragma mark - Public Methods

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    if (sender.tag != 1) {
        [self setViewMovedUp:YES fromSender:sender];
    }
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)unregisterNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark Private Methods

- (void)setViewMovedUp:(BOOL)movedUp fromSender:(UIView *)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    float heightTemp = self.height - 450;
    
    CGRect senderFrame = [self.view convertRect:sender.frame fromView:sender.superview];
    
    if (movedUp) {
        if (senderFrame.origin.y > heightTemp) {
            rect.origin.y = - (senderFrame.origin.y - heightTemp);
        }
    } else {
        rect.origin.y = 0;
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (void)keyboardWillHide {
    [self setViewMovedUp:NO fromSender:nil];
}

@end
