//
//  KeyboardToolBar.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "KeyboardToolBar.h"
#import "ZeplinHelper.h"
#import "Constants.h"

static int const kBarHeight = 50;
#define kButtonColor [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]

@interface KeyboardToolBar ()

@property (strong, nonatomic) NSArray<UITextInput> *fields;

@property (strong, nonatomic) UIBarButtonItem *btOk;
@property (strong, nonatomic) UIBarButtonItem *btBack;
@property (strong, nonatomic) UIBarButtonItem *btNext;

@end

@implementation KeyboardToolBar

#pragma mark - LifeCycle

- (id)initWithDelegate:(id<KeyboardToolBarDelegate>)delegate {
    self = [super init];
    if (self) {
        self.keyDelegate = delegate;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        [self setFrame:CGRectMake(0, 0, screenWidth, kBarHeight)];
        self.barStyle = UIBarStyleBlackTranslucent;
        self.translucent = YES;
        self.opaque = YES;
        self.barTintColor = [UIColor d_blueColor];
        
        self.btBack = [self createButton:@"Back" selector:@selector(didTapBack:)];
        self.btNext = [self createButton:@"Next" selector:@selector(didTapNext:)];
        self.btOk = [self createButton:@"Ok" selector:@selector(didTapOk:)];
    }
    return self;
}

- (void)willMoveToWindow:(UIWindow *)newWindow {
    [self fieldChanged];
}

#pragma mark - Public Methods

- (void)setColor:(UIColor *)color {
    self.backgroundColor = color;
    self.barTintColor = color;
}

- (void)setResponders:(NSArray *)fields {
    self.fields = (NSArray<UITextInput> *)fields;
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:nil
                                                                          action:nil];
    if (fields.count == 1) {
        [self setItems:@[space, self.btOk] animated:YES];
    } else if (fields.count > 1) {
        [self setItems:@[self.btBack, self.btNext, space, self.btOk] animated:YES];
    }
}

- (UIBarButtonItem *)createButton:(NSString *)title selector:(SEL)selector {
    UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithTitle:title
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:selector];
    [button setTintColor:[UIColor whiteColor]];
    [button setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16],
                                     NSForegroundColorAttributeName: [UIColor whiteColor]}
                          forState:UIControlStateNormal];
    
    [button setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16],
                                     NSForegroundColorAttributeName: [UIColor grayColor]}
                          forState:UIControlStateDisabled];
    return button;
}

- (void)button:(UIBarButtonItem *)btn enable:(BOOL)enable {
    [btn setEnabled:enable];
    UIColor *color = enable ? [UIColor whiteColor] : [UIColor grayColor];
    [btn setTitleTextAttributes:@{
                                  NSFontAttributeName           : [UIFont systemFontOfSize:16],
                                  NSForegroundColorAttributeName: color}
                       forState:UIControlStateNormal];
}

- (void)fieldChanged {
    [self setHidden:NO];
    if (self.fields.count == 0) {
        [self setHidden:YES];
    } else if (self.fields.count > 1) {
        if ([self.fields.firstObject isFirstResponder]) {
            [self button:self.btBack enable:NO];
        } else {
            [self button:self.btBack enable:YES];
        }
        
        if ([self.fields.lastObject isFirstResponder]) {
            [self button:self.btNext enable:NO];
        } else {
            [self button:self.btNext enable:YES];
        }
    }
}

#pragma mark - IBActions

- (void)didTapBack:(UIBarButtonItem *)sender {
    for (int i = 0; i < self.fields.count; i++) {
        id text = self.fields[i];
        if ([text isFirstResponder]) {
            if (i > 0) {
                [self.fields[i - 1] becomeFirstResponder];
                [self fieldChanged];
                break;
            }
        }
    }
    if (self.keyDelegate && [self.keyDelegate respondsToSelector:@selector(keyboardToolBar:didPressed:)]) {
        [self.keyDelegate keyboardToolBar:self didPressed:KeyActionBack];
    }
}

- (void)didTapNext:(UIBarButtonItem *)sender {
    for (int i = 0; i < self.fields.count; i++) {
        id text = self.fields[i];
        if ([text isFirstResponder]) {
            if (i < [self.fields count] - 1) {
                [self.fields[i + 1] becomeFirstResponder];
                [self fieldChanged];
                break;
            }
        }
    }
    
    if (self.keyDelegate && [self.keyDelegate respondsToSelector:@selector(keyboardToolBar:didPressed:)]) {
        [self.keyDelegate keyboardToolBar:self didPressed:KeyActionNext];
    }
}

- (void)didTapOk:(UIBarButtonItem*)sender {
    [self endEditing:YES];
    for (id text in self.fields) {
        if ([text isFirstResponder]){
            [text resignFirstResponder];
        }
    }
    if (self.keyDelegate && [self.keyDelegate respondsToSelector:@selector(keyboardToolBar:didPressed:)]) {
        [self.keyDelegate keyboardToolBar:self didPressed:KeyActionOk];
    }
}

@end
