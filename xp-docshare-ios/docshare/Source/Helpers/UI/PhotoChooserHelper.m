//
//  PhotoChooserHelper.m
//  Dynasty
//
//  Created by Danilo Pantani on 08/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "PhotoChooserHelper.h"

@interface PhotoChooserHelper () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) UIViewController *controller;
@property (strong, nonatomic) UIImagePickerController *picker;

@property (strong, nonatomic) id<PhotoChooserDelegate> photoDelegate;

@end

@implementation PhotoChooserHelper

#pragma mark - Lifecycle

+ (PhotoChooserHelper *)openFromController:(UIViewController<PhotoChooserDelegate> *)controller {
    PhotoChooserHelper *instance = [[PhotoChooserHelper alloc] initWithController:controller];
    [instance choosePhoto];
    return instance;
}

- (instancetype)initWithController:(UIViewController<PhotoChooserDelegate> *)controller {
    self = [super init];
    if (self) {
        self.controller = controller;
        self.photoDelegate = controller;
        self.picker = [[UIImagePickerController alloc] init];
        self.picker.allowsEditing = YES;
        self.picker.delegate = self;
        [self.view setBackgroundColor:[UIColor clearColor]];
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    return self;
}

#pragma mark - Private Methods

- (void)openPhotoLibrary {
    [self.controller presentViewController:self animated:NO completion:^{
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        } else {
            self.picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
        [self presentViewController:self.picker animated:YES completion:nil];
    }];
}

- (void)openCamera {
    [self.controller presentViewController:self animated:NO completion:^{
        self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.picker animated:YES completion:nil];
    }];
}

#pragma mark - Public Methods

- (void)choosePhoto {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select photo..."
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Camera"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            [self openCamera];
                                                        }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Photo Library"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [self openPhotoLibrary];
                                                         }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [alert addAction:cancelAction];
    
    [self.controller presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self dismissViewControllerAnimated:NO completion:^{
            if (self.photoDelegate && [self.photoDelegate respondsToSelector:@selector(didFinishPickingMedia:)]) {
                [self.photoDelegate didFinishPickingMedia:image];
            }
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

@end
