//
//  KeyboardUtil.h
//  Vimu
//
//  Created by Danilo Pantani on 22/10/16.
//  Copyright (c) 2012 Danilo Pantani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardUtil : NSObject

-(id)initFromView:(UIView*)view;
-(void)textFieldDidBeginEditing:(UITextField *)sender;
-(void)registerNotifications;
-(void)unregisterNotifications;

@end
