//
//  KeyboardToolBar.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class KeyboardToolBar;

typedef enum {
    KeyActionInvalid = 0,
    KeyActionOk = 1,
    KeyActionBack,
    KeyActionNext
}KeyAction;

@protocol KeyboardToolBarDelegate <NSObject>

- (void)keyboardToolBar:(KeyboardToolBar *)keyToolbar didPressed:(KeyAction)action;

@end

@interface KeyboardToolBar : UIToolbar

@property (nonatomic) id<KeyboardToolBarDelegate> keyDelegate;

- (id)initWithDelegate:(id<KeyboardToolBarDelegate>)delegate;

- (void)fieldChanged;
- (void)setResponders:(NSArray *)fields;

@end
