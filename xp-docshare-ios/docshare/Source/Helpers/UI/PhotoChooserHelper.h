//
//  PhotoChooserHelper.h
//  Dynasty
//
//  Created by Danilo Pantani on 08/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PhotoChooserDelegate <PresenterProtocol>

@required
- (void)didFinishPickingMedia:(UIImage *)photo;

@end

@interface PhotoChooserHelper : UIViewController

+ (PhotoChooserHelper *)openFromController:(UIViewController<PhotoChooserDelegate> *)controller;

@end
