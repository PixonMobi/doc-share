//
//  BaseRequest.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BaseRequest.h"
#import "SVProgressHUD.h"
#import "OperationQueueManager.h"
#import "NSDictionary+JSON.h"
#import "User.h"
#import "Utils.h"
#import <objc/runtime.h>

@interface BaseRequest ()

@property (nonatomic, copy) void (^completion)(id result, BaseResponse *message);
@property (nonatomic, assign) BOOL hasLoading;

@end

@implementation BaseRequest
/*
 We need to do old school synthesizing as the compiler has trouble creating the internal ivars.
 */
@synthesize ready = _ready;
@synthesize executing = _executing;
@synthesize finished = _finished;

#pragma mark - Init

- (instancetype)init {
    self = [super init];
    if (self) {
        self.ready = YES;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    BaseRequest *copy = [[[self class] allocWithZone:zone] init];
    copy.completion = [self.completion copyWithZone:zone];
    copy.hasLoading = self.hasLoading;
    return copy;
}

#pragma mark - State

- (void)setReady:(BOOL)ready {
    if (_ready != ready) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isReady))];
        _ready = ready;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isReady))];
    }
}

- (BOOL)isReady {
    return _ready;
}

- (void)setExecuting:(BOOL)executing {
    if (_executing != executing) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
        _executing = executing;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
    }
}

- (BOOL)isExecuting {
    return _executing;
}

- (void)setFinished:(BOOL)finished {
    if (_finished != finished) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
        _finished = finished;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
    }
}

- (BOOL)isFinished {
    return _finished;
}

- (BOOL)isAsynchronous {
    return YES;
}

#pragma mark - Params

- (NSDictionary *)params {
    return nil;
}

- (NSString *)endpoint {
    return nil;
}

- (NSString *)HTTPMethod {
    return @"GET";
}

#pragma mark - Control

- (void)startWithCompletion:(void (^)(id result, BaseResponse *response))completion {
    [self startWithLoading:YES andCompletion:completion];
}

- (void)startWithoutLoadingAndCompletion:(void (^)(id result, BaseResponse *response))completion {
    [self startWithLoading:NO andCompletion:completion];
}

- (void)startWithLoading:(BOOL)loading andCompletion:(void (^)(id result, BaseResponse *response))completion {
    [UIApplication sharedApplication].windows[0].userInteractionEnabled = !loading;
    self.completion = completion;
    self.hasLoading = loading;
    BaseRequest *request = [self copy];
    request.completion = completion;
    [[OperationQueueManager sharedInstance] addOperation:request];
}

- (void)start {
    if (self.isExecuting) {
        return;
    }
    
    if (self.hasLoading) {
        [SVProgressHUD showWithStatus:@"Waiting..."];
    }
    
    self.ready = NO;
    self.executing = YES;
    self.finished = NO;
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSString *URLString = [NSString stringWithFormat:kApiUrl];
    if ([self endpoint]) {
        URLString = [URLString stringByAppendingString:[self endpoint]];
    }
    
    NSURL *url = [NSURL URLWithString:URLString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:40.0];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    if ([User token]) {
        [request setValue:[User token] forHTTPHeaderField:kXToken];
    }
    
    if ([self params]) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:[self params]
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        [request setHTTPBody:postData];
    }
    
    [request setHTTPMethod:[self HTTPMethod]];
    
    [self logRequest:request];
    
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    NSURLSessionDataTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
     {
         NSTimeInterval timeResult = [[NSDate date] timeIntervalSince1970] - timeInSeconds;
         
         [SVProgressHUD dismiss];
         if (!error) {
             id responseJSON = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error: nil];
             
             [self logResponse:response result:responseJSON time:timeResult];
             if (responseJSON) {
                 BaseResponse *baseResult = [[BaseResponse alloc] initWithData:responseJSON];
                 
                 if (baseResult.success && self.hasLoading) {
                     [SVProgressHUD showSuccessWithStatus:@"Success!"];
                 }
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                     [Utils asyncMain:^{
                         self.completion(responseJSON, baseResult);
                         [UIApplication sharedApplication].windows[0].userInteractionEnabled = YES;
                     }];
                 }];
             }else{
                 if (self.hasLoading) {
                     [SVProgressHUD showErrorWithStatus:@"Connection Failed! Try again later."];
                 }
                 BaseResponse *baseResult = [[BaseResponse alloc] initWithSuccess:@NO
                                                                          message:[ApiMessage defaultErrorMessage]];
                 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                     [Utils asyncMain:^{
                         self.completion(nil, baseResult);
                         [UIApplication sharedApplication].windows[0].userInteractionEnabled = YES;
                     }];
                 }];
             }
         }else{
             [self logResponse:response result:nil time:timeResult];
             if (self.hasLoading) {
                 [SVProgressHUD showErrorWithStatus:@"Connection Failed! Try again later."];
             }
             BaseResponse *baseResult = [[BaseResponse alloc] initWithSuccess:@NO
                                                                      message:[ApiMessage defaultErrorMessage:error.localizedDescription]];
             [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                 [Utils asyncMain:^{
                     self.completion(nil, baseResult);
                     [UIApplication sharedApplication].windows[0].userInteractionEnabled = YES;
                 }];
             }];
         }
         [self finish];
     }];
    
    [task resume];
}

- (void)finish {
    if (self.executing) {
        self.executing = NO;
        self.finished = YES;
    }
}

- (void)cancel {
    [super cancel];
    [self finish];
}

- (void)logRequest:(NSMutableURLRequest *)request {
    NSString *log = @"\n\n===================== REQUEST =====================\n";
    log = [log stringByAppendingFormat:@"| %@ -> %@\n",[request HTTPMethod],[request URL].absoluteString];
    
    if ([request allHTTPHeaderFields].count > 0) {
        log = [log stringByAppendingFormat:@"| HEADERS:\n"];
        for (NSString *key in [request allHTTPHeaderFields].allKeys) {
            log = [log stringByAppendingFormat:@"|| %@ : %@\n", key, [request allHTTPHeaderFields][key]];
        }
    }
    
    if ([request HTTPBody]) {
        NSDictionary *requestJSON = [NSJSONSerialization JSONObjectWithData:[request HTTPBody] options: NSJSONReadingMutableContainers error: nil];
        log = [log stringByAppendingFormat:@"| Body -> %@\n", [requestJSON jsonStringWithPrettyPrint:YES]];
    }
    log = [log stringByAppendingFormat:@"=================== END REQUEST ===================\n"];
    DDLogDebug(@"%@", log);
}

- (void)logResponse:(NSURLResponse *)response result:(NSDictionary *)result time:(NSTimeInterval)time {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    NSString *log = [NSString stringWithFormat:@"\n\n===================== RESPONSE (%lu) in %f(ms) =====================\n", (long)[httpResponse statusCode], time];
    
    log = [log stringByAppendingFormat:@"| Endpoint -> %@\n",[httpResponse URL].absoluteString];
    
    if ([httpResponse allHeaderFields].count > 0) {
        log = [log stringByAppendingFormat:@"| HEADERS:\n"];
        for (NSString *key in [httpResponse allHeaderFields].allKeys) {
            log = [log stringByAppendingFormat:@"|| %@ : %@\n", key, [httpResponse allHeaderFields][key]];
        }
    }
    
    if (result) {
        log = [log stringByAppendingFormat:@"| Result -> %@\n", [result jsonStringWithPrettyPrint:YES]];
    }
    log = [log stringByAppendingFormat:@"=================== END RESPONSE ===================\n"];
    DDLogDebug(@"%@", log);
}

@end
