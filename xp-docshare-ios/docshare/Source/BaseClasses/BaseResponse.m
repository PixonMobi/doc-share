//
//  BaseResponse.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BaseResponse.h"
#import "Utils.h"

@implementation BaseResponse

#pragma mark - Init

- (instancetype)initWithSuccess:(NSNumber *)success
                        message:(ApiMessage *)message {
    self = [super init];
    if (self) {
        self.message = message;
        self.success = [success boolValue];
    }
    return self;
}

- (instancetype)initWithData:(NSDictionary *)data {
    
    self = [super init];
    if (self) {
        self.message = [[ApiMessage alloc] initWithData:data[kPMessage]];
        self.success = [Utils booleanFromValue:data[kPMessageSuccess] defaultBoolean:NO];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    BaseResponse *copy = [[[self class] allocWithZone:zone] init];
    copy.message = [self.message copyWithZone:zone];
    copy.success = self.success;
    return copy;
}

@end
