//
//  BaseViewController.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardToolBar.h"
#import "KeyboardUtil.h"

@interface BaseViewController : UIViewController

@property (strong, nonatomic) KeyboardToolBar *keyTool;
@property (strong, nonatomic) KeyboardUtil *keyUtil;

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

@end
