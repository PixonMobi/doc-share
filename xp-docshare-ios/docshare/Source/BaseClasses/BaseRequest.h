//
//  BaseRequest.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiMessage.h"

@class BaseResponse;

/**
 Abstract class for asynchronus operations.
 */
@interface BaseRequest : NSOperation <NSCopying>

/**
 Start http request
 
 @param completion The return block from request.
 */
- (void)startWithCompletion:(void (^)(id result, BaseResponse *response))completion;

/**
 Start http request without loading HUD
 
 @param completion The return block from request.
 */
- (void)startWithoutLoadingAndCompletion:(void (^)(id result, BaseResponse *response))completion;

/**
 Start http request
 
 @param loading With you wanna show loading
 @param completion The return block from request.
 */
- (void)startWithLoading:(BOOL)loading andCompletion:(void (^)(id result, BaseResponse *response))completion;

/**
 Finishes the execution of the operation.
 
 @note - This shouldn’t be called externally as this is used internally by subclasses. To cancel an operation use cancel instead.
 */
- (void)finish;

@end
