//
//  BasePresenter.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PresenterProtocol <NSObject>

@optional
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

@end

@interface BasePresenter : NSObject

@end
