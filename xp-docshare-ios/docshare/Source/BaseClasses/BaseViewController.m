//
//  BaseViewController.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BaseViewController.h"
#import "Utils.h"

@interface BaseViewController () <KeyboardToolBarDelegate, UIGestureRecognizerDelegate>


@end

@implementation BaseViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.keyTool = [[KeyboardToolBar alloc] initWithDelegate:self];
    self.keyUtil = [[KeyboardUtil alloc] initFromView:self.view];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.keyUtil registerNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
    [self.keyUtil unregisterNotifications];
}

//- (void)viewDidDisappear:(BOOL)animated {
//    [super viewDidDisappear:animated];
//    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
//}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - KeyboardToolBarDelegate

- (void)keyboardToolBar:(KeyboardToolBar *)keyToolbar didPressed:(KeyAction)action {

}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 2) {
        textField.text = [textField.text stringByAppendingString:string];
        textField.text = [Utils currencyMaskWithText:textField.text andRange:range];
        return [Utils returnForShouldChangeCharactersInRange:range];
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.inputAccessoryView = self.keyTool;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.keyUtil textFieldDidBeginEditing:textField];
    [self.keyTool fieldChanged];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Touch Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - Actions

- (IBAction)didTapBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapClose:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Presenters

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    [Utils showAlertWithTitle:title
                      message:message
               fromController:self];
}

@end
