//
//  BaseResponse.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiMessage.h"

@interface BaseResponse : NSObject <NSCopying>

@property (nonatomic, strong) ApiMessage *message;
@property (nonatomic, assign) BOOL success;

#pragma mark - Init

- (instancetype)initWithSuccess:(NSNumber *)success
                        message:(ApiMessage *)message;

- (instancetype)initWithData:(NSDictionary *)data;

@end
