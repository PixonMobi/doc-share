//
//  Post.h
//  Dynasty
//
//  Created by Danilo Pantani on 21/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *fileUrl;
@property (nonatomic, strong) NSString *coverUrl;
@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSNumber *active;
@property (nonatomic, strong) NSNumber *slider;
@property (nonatomic, strong) NSArray<NSNumber *> *roles;

- (instancetype)initWithName:(NSString *)name
                        date:(NSString *)date
                     fileUrl:(NSString *)fileUrl
                    coverUrl:(NSString *)coverUrl
                        type:(NSNumber *)type
                      active:(NSNumber *)active
                      slider:(NSNumber *)slider
                       roles:(NSArray<NSNumber *> *)roles;

- (instancetype)initWithData:(NSDictionary *)data;

@end
