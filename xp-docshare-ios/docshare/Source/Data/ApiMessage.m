//
//  ApiMessage.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "ApiMessage.h"
#import "Constants.h"
#import "Utils.h"

@interface ApiMessage ()

@end

@implementation ApiMessage

#pragma mark - Init

- (instancetype)initWithId:(NSNumber *)messageId
                     title:(NSString *)title
                   message:(NSString *)message {
    
    self = [super init];
    if (self) {
        self.messageId = messageId;
        self.title = title;
        self.message = message;
    }
    return self;
}

- (instancetype)initWithData:(NSDictionary *)data {
    
    self = [super init];
    if (self) {
        self.messageId = [Utils numberFromValue:data[kPMessageId]];
        self.title = [Utils stringFromValue:data[kPMessageTitle]];
        self.message = [Utils stringFromValue:data[kPMessage]];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    ApiMessage *copy = [[[self class] allocWithZone:zone] init];
    copy.messageId = [self.messageId copyWithZone:zone];
    copy.title = [self.title copyWithZone:zone];
    copy.message = [self.message copyWithZone:zone];
    return copy;
}

#pragma mark - Default Messages

+ (ApiMessage *)defaultErrorMessage {
    return [[ApiMessage alloc] initWithId:@0
                                    title:@"Dynasty"
                                  message:@"Something went wrong on our servers, try again later!"];
}

+ (ApiMessage *)defaultErrorMessage:(NSString *)message {
    return [[ApiMessage alloc] initWithId:@0
                                    title:@"Dynasty"
                                  message:message];
}

@end
