//
//  ApiMessage.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiMessage : NSObject <NSCopying>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSNumber *messageId;

- (instancetype)initWithId:(NSNumber *)messageId
                     title:(NSString *)title
                   message:(NSString *)message;

- (instancetype)initWithData:(NSDictionary *)data;

+ (ApiMessage *)defaultErrorMessage;

+ (ApiMessage *)defaultErrorMessage:(NSString *)message;

@end
