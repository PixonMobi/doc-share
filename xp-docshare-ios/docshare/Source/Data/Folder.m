//
//  Folder.m
//  Dynasty
//
//  Created by Danilo Pantani on 21/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "Folder.h"
#import "Utils.h"

@implementation Folder

- (instancetype)initWithFolder:(NSArray<Folder *> *)folder
                          post:(NSArray<Post *> *)post {
    self = [super init];
    if (self) {
        self.folder = folder;
        self.post = post;
    }
    return self;
}

- (instancetype)initWithData:(NSDictionary *)data {
    self = [super init];
    if (self) {
       
    }
    return self;
}

@end
