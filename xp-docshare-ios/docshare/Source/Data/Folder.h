//
//  Folder.h
//  Dynasty
//
//  Created by Danilo Pantani on 21/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Post;

@interface Folder : NSObject

@property (nonatomic, strong) NSArray<Folder *> *folder;
@property (nonatomic, strong) NSArray<Post *> *post;

- (instancetype)initWithFolder:(NSArray<Folder *> *)folder
                          post:(NSArray<Post *> *)post;

- (instancetype)initWithData:(NSDictionary *)data;

@end
