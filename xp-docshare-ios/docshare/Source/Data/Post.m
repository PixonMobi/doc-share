//
//  Post.m
//  Dynasty
//
//  Created by Danilo Pantani on 21/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "Post.h"
#import "Utils.h"

@implementation Post

- (instancetype)initWithName:(NSString *)name
                        date:(NSString *)date
                     fileUrl:(NSString *)fileUrl
                    coverUrl:(NSString *)coverUrl
                        type:(NSNumber *)type
                      active:(NSNumber *)active
                      slider:(NSNumber *)slider
                       roles:(NSArray<NSNumber *> *)roles {
    self = [super init];
    if (self) {
        self.name = name;
        self.date = date;
        self.fileUrl = fileUrl;
        self.coverUrl = coverUrl;
        self.type = type;
        self.active = active;
        self.slider = slider;
        self.roles = roles;
    }
    return self;
}

- (instancetype)initWithData:(NSDictionary *)data {
    self = [super init];
    if (self) {
        
        self.name = [Utils stringFromValue:data[kPName]];
        self.date = [Utils stringFromValue:data[kPDate]];
        self.fileUrl = [Utils stringFromValue:data[kPCoverUrl]];
        self.coverUrl = [Utils stringFromValue:data[kPCoverUrl]];
        self.type = [Utils numberFromValue:data[kPType]];
        self.active = [Utils numberFromValue:data[kPActive]];
        self.slider = [Utils numberFromValue:data[kPSlider]];
                
        NSMutableArray<NSNumber *> *roles = [NSMutableArray array];
        for (NSNumber *temp in data[kPRoles]) {
            [roles addObject:temp];
        }
        self.roles = roles;
    }
    return self;
}

@end
