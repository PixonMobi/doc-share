//
//  AppDelegate.h
//  docshare
//
//  Created by Danilo Pantani on 25/12/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

