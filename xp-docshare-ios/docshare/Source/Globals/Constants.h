//Paths
static NSString *const kCacheImagePath = @"/Library/Caches/Images";
static NSString *const kCachePath = @"/Library/Caches";
static NSString *const kHost = @"https://www.google.com";

//Services Mock
static NSString *const kApiUrl = @"http://pixon.mobi/xp/";
static NSString *const kApiLogin = @"api/login.php";
static NSString *const kApiRecoveryPass = @"api/recovery_pass.php";
static NSString *const kApiLogout = @"api/logout.php";
static NSString *const kApiEditUser = @"api/edit_user.php";
static NSString *const kApiPosts = @"api/get_posts.php";

// Header Params
static NSString *const kXToken = @"x-access-token";

//Params
static NSString *const kPMessageId = @"messageId";
static NSString *const kPMessage = @"message";
static NSString *const kPMessageTitle = @"title";
static NSString *const kPMessageSuccess = @"success";
static NSString *const kPEmail = @"email";
static NSString *const kPPassword = @"password";
static NSString *const kPName = @"name";
static NSString *const kPIdNumber = @"idNumber";
static NSString *const kPPhone = @"phone";
static NSString *const kPDevice = @"device";
static NSString *const kPPushToken = @"pushToken";
static NSString *const kPData = @"data";
static NSString *const kPId = @"id";
static NSString *const kPToken = @"token";
static NSString *const kPPhoto = @"photo";
static NSString *const kPPhotoStr = @"photoStr";
static NSString *const kPStartDate = @"startDate";
static NSString *const kPEndDate = @"endDate";
static NSString *const kPDate = @"date";
static NSString *const kPFileUrl = @"fileUrl";
static NSString *const kPCoverUrl = @"coverUrl";
static NSString *const kPType = @"type";
static NSString *const kPActive = @"active";
static NSString *const kPSlider = @"slider";
static NSString *const kPRoles = @"roles";
static NSString *const kPUserId = @"userId";

//UserDefaults
static NSString *const kUserDefaultsName = @"name";
static NSString *const kUserDefaultsEmail = @"email";
static NSString *const kUserDefaultsIdNumber = @"idNumber";
static NSString *const kUserDefaultsPhone = @"phone";
static NSString *const kUserDefaultsPhotoUrl = @"photoUrl";

//Date Formatters
static NSString *const kJSONDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

//Notifications
static NSString *const DyNotificationDidUpdateBalance = @"DyNotificationDidUpdateBalance";

//Constants
static int const kServiceTimeout = 15000;
static int const kCurrencyMaxLenght = 15;

//Fonts
static NSString *const kExo2_BoldItalic = @"Exo2-BoldItalic";

//Localizable Strings
#define LString(key) [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:@"Localizable"]

//Fonts
#define kFont(X,Y) [UIFont fontWithName:X size:Y]

typedef enum {
    ETransferTypeInvalid = 0,
    ETransferTypeSend = 1,
    ETransferTypeReceive
}ETransferType;

typedef enum {
    EDeviceTypeInvalid = 0,
    EDeviceTypeIOS = 1,
    EDeviceTypeAndroid
}EDeviceType;
