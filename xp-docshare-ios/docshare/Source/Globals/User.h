//
//  User.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : BaseResponse

+ (User *)currentUser;

+ (void)setData:(NSDictionary *)data;

+ (void)setPushToken:(NSString *)pushToken;
+ (NSString *)pushToken;

+ (void)setToken:(NSString *)token;
+ (NSString *)token;

+ (void)setEmail:(NSString *)email;
+ (NSString *)email;

+ (void)setName:(NSString *)name;
+ (NSString *)name;

+ (void)setPhone:(NSString *)phone;
+ (NSString *)phone;

+ (void)setPhotoUrl:(NSString *)photoUrl;
+ (NSString *)photoUrl;

+(void)logout;

+(void)getStandardDefaults;
+(void)saveStandardDefaults;

+(void)deleteAllImages;

@end
