//
//  User.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "User.h"
#import "Utils.h"

@interface User ()

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *photoUrl;

@property (nonatomic, strong) NSString *pushToken;

@end

@implementation User

#pragma mark - LifeCycle

static User *_shared = nil;

+ (User*)currentUser {
    @synchronized(self) {
        if (_shared == nil) {
            _shared = [[self alloc]init];
        }
        return _shared;
    }
}

- (id)init {
    self = [super init];
    if (self != nil) {
        
    }
    return self;
}

#pragma mark - Private Setters Methods

- (void)setData:(NSDictionary *)data {
    self.token = [Utils stringFromValue:data[kPToken]];
    self.email = [Utils stringFromValue:data[kPEmail]];
    self.name = [Utils stringFromValue:data[kPName]];
    self.phone = [Utils stringFromValue:data[kPPhone]];
    self.photoUrl = [Utils stringFromValue:data[kPPhoto]];
}

#pragma mark - Public Setters Methods

+ (void)setData:(NSDictionary *)data {
    [[User currentUser] setData:data];
}

+ (void)setPushToken:(NSString *)pushToken {
    [User currentUser].pushToken = pushToken;
}

+ (void)setToken:(NSString *)token {
    [User currentUser].token = token;
}

+ (void)setName:(NSString *)name {
    [User currentUser].name = name;
}

+ (void)setEmail:(NSString *)email {
    [User currentUser].email = email;
}

+ (void)setPhone:(NSString *)phone {
    [User currentUser].phone = phone;
}

+ (void)setPhotoUrl:(NSString *)photoUrl {
    [User currentUser].photoUrl = photoUrl;
}

#pragma mark - Public Getters Methods

+ (NSString *)token {
    return [User currentUser].token;
}

+ (NSString *)pushToken {
    return [User currentUser].pushToken;
}

+ (NSString *)name {
    return [User currentUser].name;
}

+ (NSString *)email {
    return [User currentUser].email;
}

+ (NSString *)phone {
    return [User currentUser].phone;
}

+ (NSString *)photoUrl {
    return [User currentUser].photoUrl;
}

#pragma mark - Public Methods

+ (void)saveStandardDefaults {
    [[User currentUser] saveStandardDefaults];
}

+ (void)getStandardDefaults {
    [[User currentUser] getStandardDefaults];
}

+ (void)logout {
    [[User currentUser] logout];
}

+ (void)deleteAllImages {
    [[User currentUser] deleteAllImages];
}

#pragma mark - StandardDefaults Methods

- (void)saveStandardDefaults {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setObject:self.name forKey:kUserDefaultsName];
    [standardUserDefaults setObject:self.email forKey:kUserDefaultsEmail];
    [standardUserDefaults setObject:self.phone forKey:kUserDefaultsPhone];
    [standardUserDefaults setObject:self.photoUrl forKey:kUserDefaultsPhotoUrl];
    [standardUserDefaults synchronize];
}

- (void)getStandardDefaults {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    self.name = [standardUserDefaults objectForKey:kUserDefaultsName];
    self.email = [standardUserDefaults objectForKey:kUserDefaultsEmail];
    self.phone = [standardUserDefaults objectForKey:kUserDefaultsPhone];
    self.photoUrl = [standardUserDefaults objectForKey:kUserDefaultsPhotoUrl];
}

#pragma mark - Logout Methods

- (void)logout {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if ([standardUserDefaults objectForKey:kUserDefaultsName]) {
        [standardUserDefaults removeObjectForKey:kUserDefaultsName];
    }
    if ([standardUserDefaults objectForKey:kUserDefaultsEmail]) {
        [standardUserDefaults removeObjectForKey:kUserDefaultsEmail];
    }
    if ([standardUserDefaults objectForKey:kUserDefaultsIdNumber]) {
        [standardUserDefaults removeObjectForKey:kUserDefaultsIdNumber];
    }
    if ([standardUserDefaults objectForKey:kUserDefaultsPhone]) {
        [standardUserDefaults removeObjectForKey:kUserDefaultsPhone];
    }
    if ([standardUserDefaults objectForKey:kUserDefaultsPhotoUrl]) {
        [standardUserDefaults removeObjectForKey:kUserDefaultsPhotoUrl];
    }
    
    [standardUserDefaults synchronize];
    
    self.name = nil;
    self.email = nil;
    self.phone = nil;
    self.photoUrl = nil;
    self.pushToken = nil;
    self.token = nil;

    [self deleteAllImages];
}

- (void)deleteAllImages {
    NSString *folderPath = [NSHomeDirectory() stringByAppendingString:kCacheImagePath];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSArray *directoryContent = [fileManager contentsOfDirectoryAtPath:folderPath error:NULL];
    
    NSError* error;
    NSString* fullFilePath = nil;
    
    for(NSString* fileName in directoryContent) {
        fullFilePath = [folderPath stringByAppendingPathComponent:fileName];
        [fileManager removeItemAtPath:fullFilePath error:&error];
        if(error) {
            DDLogError(@"Error: %@",error);
        }
    }
}

@end
