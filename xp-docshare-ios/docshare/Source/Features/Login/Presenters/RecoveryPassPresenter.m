//
//  RecoveryPassPresenter.m
//  Dynasty
//
//  Created by Danilo Pantani on 05/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "RecoveryPassPresenter.h"
#import "RecoveryPassRequest.h"

@interface RecoveryPassPresenter ()

@property (nonatomic) UIViewController<RecoveryPassProtocol> *controller;

@end

@implementation RecoveryPassPresenter

#pragma mark - Lifecycle

- (instancetype)initWithController:(UIViewController<RecoveryPassProtocol> *)controller {
    self = [super init];
    if (self) {
        self.controller = controller;
    }
    return self;
}

#pragma mark - Public Methods

- (void)recoveryPassWithEmail:(NSString *)email {
    RecoveryPassRequest *request = [[RecoveryPassRequest alloc] initWithEmail:email];
    [request startWithCompletion:^(id result, BaseResponse *response) {
        if ([response success]) {
            if (self.controller && [self.controller respondsToSelector:@selector(recoveryDidSuccess)]) {
                [self.controller recoveryDidSuccess];
            }
        } else {
            ApiMessage *message = response.message;
            if (!message) {
                message = [ApiMessage defaultErrorMessage];
            }
            if (self.controller && [self.controller respondsToSelector:@selector(showAlertWithTitle:message:)]) {
                [self.controller showAlertWithTitle:message.title
                                            message:message.message];
            }
        }
    }];
}

@end
