//
//  LoginPresenter.h
//  Dynasty
//
//  Created by Danilo Pantani on 05/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BasePresenter.h"

@protocol LoginProtocol <PresenterProtocol>

@required
- (void)loginDidSuccess;
- (void)loginDidFailure;

@end

@interface LoginPresenter : BasePresenter

- (instancetype)initWithController:(UIViewController<LoginProtocol> *)controller;

- (void)loginWithEmail:(NSString *)email
           andPassword:(NSString *)password;

@end
