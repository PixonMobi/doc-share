//
//  LoginPresenter.m
//  Dynasty
//
//  Created by Danilo Pantani on 05/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "LoginPresenter.h"
#import "NSString+Utils.h"
#import "LoginRequest.h"
#import "User.h"

@interface LoginPresenter ()

@property (nonatomic) UIViewController<LoginProtocol> *controller;

@end

@implementation LoginPresenter

#pragma mark - Lifecycle

- (instancetype)initWithController:(UIViewController<LoginProtocol> *)controller {
    self = [super init];
    if (self) {
        self.controller = controller;
    }
    return self;
}

#pragma mark - Public Methods

- (void)loginWithEmail:(NSString *)email
           andPassword:(NSString *)password {
    LoginRequest *request = [[LoginRequest alloc] initWithEmail:email
                                                       password:password
//                                                       password:[password sha256]
                                                         device:@(EDeviceTypeIOS)
                                                      pushToken:[User pushToken]];
    [request startWithCompletion:^(id result, BaseResponse *response) {
        if ([response success]) {
            [User setData:result[kPData]];
            if (self.controller && [self.controller respondsToSelector:@selector(loginDidSuccess)]) {
                [self.controller loginDidSuccess];
            }
        } else {
            if (self.controller && [self.controller respondsToSelector:@selector(loginDidFailure)]) {
                [self.controller loginDidFailure];
            }
        }
    }];
}

@end
