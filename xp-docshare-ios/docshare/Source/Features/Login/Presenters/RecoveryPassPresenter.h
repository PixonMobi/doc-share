//
//  RecoveryPassPresenter.h
//  Dynasty
//
//  Created by Danilo Pantani on 05/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BasePresenter.h"

@protocol RecoveryPassProtocol <PresenterProtocol>

@required
- (void)recoveryDidSuccess;

@end

@interface RecoveryPassPresenter : BasePresenter

- (instancetype)initWithController:(UIViewController<RecoveryPassProtocol> *)controller;

- (void)recoveryPassWithEmail:(NSString *)email;

@end
