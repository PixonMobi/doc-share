//
//  RecoveryPassRequest.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "RecoveryPassRequest.h"

@implementation RecoveryPassRequest

#pragma mark - Init

- (instancetype)initWithEmail:(NSString *)email {
    self = [super init];
    if (self) {
        self.email = email;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    RecoveryPassRequest *copy = [[[self class] allocWithZone:zone] init];
    copy.email = [self.email copyWithZone:zone];
    return copy;
}

#pragma mark - Request

- (NSString *)endpoint {
    return kApiRecoveryPass;
}

- (NSString *)HTTPMethod {
    return @"POST";
}

- (NSDictionary *)params {
    return @{kPEmail : self.email};
}

@end
