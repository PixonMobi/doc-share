//
//  LoginRequest.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "LoginRequest.h"

@implementation LoginRequest

#pragma mark - Init

- (instancetype)initWithEmail:(NSString *)email
                     password:(NSString *)password
                       device:(NSNumber *)device
                    pushToken:(NSString *)pushToken {
    self = [super init];
    if (self) {
        self.password = password;
        self.email = email;
        self.device = device;
        self.pushToken = pushToken;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    LoginRequest *copy = [[[self class] allocWithZone:zone] init];
    copy.password = [self.password copyWithZone:zone];
    copy.email = [self.email copyWithZone:zone];
    copy.device = [self.device copyWithZone:zone];
    copy.pushToken = [self.pushToken copyWithZone:zone];
    return copy;
}

#pragma mark - Request

- (NSString *)endpoint {
    return kApiLogin;
}

- (NSString *)HTTPMethod {
    return @"POST";
}

- (NSDictionary *)params {
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:
                                @{kPEmail        : self.email,
                                  kPPassword     : self.password,
                                  kPDevice       : self.device}];
    
    if (self.pushToken) {
        [dic setValue:self.pushToken forKey:kPPushToken];
    }
    
    return dic;
}

@end
