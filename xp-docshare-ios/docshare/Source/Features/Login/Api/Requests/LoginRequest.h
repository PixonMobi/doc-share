//
//  LoginRequest.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BaseRequest.h"

@interface LoginRequest : BaseRequest <NSCopying>

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSNumber *device;
@property (nonatomic, strong) NSString *pushToken;

- (instancetype)initWithEmail:(NSString *)email
                     password:(NSString *)password
                       device:(NSNumber *)device
                    pushToken:(NSString *)pushToken;

@end
