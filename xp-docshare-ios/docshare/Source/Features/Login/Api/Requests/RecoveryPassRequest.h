//
//  RecoveryPassRequest.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BaseRequest.h"

@interface RecoveryPassRequest : BaseRequest <NSCopying>

@property (nonatomic, strong) NSString *email;

- (instancetype)initWithEmail:(NSString *)email;

@end
