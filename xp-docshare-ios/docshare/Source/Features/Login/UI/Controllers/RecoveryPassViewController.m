//
//  RecoveryPassViewController.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "RecoveryPassViewController.h"
#import "RecoveryPassPresenter.h"
#import "NSString+Utils.h"
#import "Utils.h"

@interface RecoveryPassViewController () <RecoveryPassProtocol>

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (nonatomic, strong) RecoveryPassPresenter *presenter;

@end

@implementation RecoveryPassViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.presenter = [[RecoveryPassPresenter alloc] initWithController:self];
    [self.keyTool setResponders:@[self.txtEmail]];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - Presenters

- (void)recoveryDidSuccess {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [Utils showAlert:@"You will receive an email to recover your password!" fromController:self];
}

#pragma mark - Actions

- (IBAction)didTapRetrieve:(UIButton *)sender {
    if (![self.txtEmail.text isNotEmpty] && ![self.txtEmail.text isEmail]) {
        [Utils showAlert:@"Enter the \"E-mail\" field correctly" fromController:self];
        return;
    }
    
    [self.presenter recoveryPassWithEmail:self.txtEmail.text];
}

- (IBAction)didTapTryAgain:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
