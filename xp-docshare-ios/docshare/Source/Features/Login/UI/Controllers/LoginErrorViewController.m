//
//  LoginErrorViewController.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "LoginErrorViewController.h"

@interface LoginErrorViewController ()

@end

@implementation LoginErrorViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

#pragma mark - Actions

- (IBAction)didTapTryAgain:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
