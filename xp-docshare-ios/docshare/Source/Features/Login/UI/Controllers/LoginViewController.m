//
//  LoginViewController.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginPresenter.h"
#import "KeyboardToolBar.h"
#import "NSString+Utils.h"
#import "Utils.h"
#import "User.h"

@interface LoginViewController () <LoginProtocol>

@property (nonatomic, strong) LoginPresenter *presenter;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end

@implementation LoginViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.presenter = [[LoginPresenter alloc] initWithController:self];
    [self.keyTool setResponders:@[self.txtEmail,
                                  self.txtPassword]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.txtEmail.text = [User email] ? [User email] : @"";
    self.txtPassword.text = @"";
    
#ifdef DEBUG
    self.txtEmail.text = @"dpantani@hotmail.com";
    self.txtPassword.text = @"1111";
#endif

    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Presenters

- (void)loginDidSuccess {
    [self performSegueWithIdentifier:@"DashboardSegue" sender:self];
}

- (void)loginDidFailure {
    [self performSegueWithIdentifier:@"LoginErrorSegue" sender:self];
}

#pragma mark - Actions

- (IBAction)didTapLogin:(UIButton *)sender {
    if (![self.txtEmail.text isNotEmpty] && ![self.txtEmail.text isEmail]) {
        [Utils showAlert:@"Enter the \"Email\" field correctly" fromController:self];
        return;
    }
    
    if (![self.txtPassword.text isNotEmpty]) {
        [Utils showAlert:@"Enter the \"Password\" field correctly" fromController:self];
        return;
    }
    
    [self.presenter loginWithEmail:self.txtEmail.text
                       andPassword:self.txtPassword.text];
}

@end
