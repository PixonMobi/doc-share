//
//  DashboardPresenter.m
//  Dynasty
//
//  Created by Danilo Pantani on 05/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "DashboardPresenter.h"
#import "PostsRequest.h"
#import "Utils.h"
#import "User.h"

@interface DashboardPresenter ()

@property (nonatomic) UIViewController<DashboardProtocol> *controller;

@end

@implementation DashboardPresenter

#pragma mark - Lifecycle

- (instancetype)initWithController:(UIViewController<DashboardProtocol> *)controller {
    self = [super init];
    if (self) {
        self.controller = controller;
    }
    return self;
}

#pragma mark - Public Methods

- (void)fetchPostsFromUser:(NSNumber *)userId {
    
    PostsRequest *request = [[PostsRequest alloc] initWithUser:userId];
    [request startWithLoading:YES andCompletion:^(id result, BaseResponse *response) {
        if ([response success]) {
            
            
            if (self.controller && [self.controller respondsToSelector:@selector(didFetchPosts:)]) {
//                [self.controller didFetchPosts:result];
            }
        } else {
            ApiMessage *message = response.message;
            if (!message) {
                message = [ApiMessage defaultErrorMessage];
            }
            if (self.controller && [self.controller respondsToSelector:@selector(showAlertWithTitle:message:)]) {
                [self.controller showAlertWithTitle:message.title
                                            message:message.message];
            }
        }
    }];
}

@end
