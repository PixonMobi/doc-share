//
//  DashboardPresenter.h
//  Dynasty
//
//  Created by Danilo Pantani on 05/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BasePresenter.h"
@class Post;

@protocol DashboardProtocol <PresenterProtocol>

@required
- (void)didFetchPosts:(Post *)posts;

@end

@interface DashboardPresenter : BasePresenter

- (instancetype)initWithController:(UIViewController<DashboardProtocol> *)controller;

- (void)fetchPostsFromUser:(NSNumber *)userId;

@end
