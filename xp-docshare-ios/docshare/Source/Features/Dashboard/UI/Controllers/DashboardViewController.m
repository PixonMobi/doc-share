//
//  DashboardViewController.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "DashboardViewController.h"
#import "PostTableViewCell.h"
#import "DashboardPresenter.h"
#import "Crashlytics+Utils.h"
#import "Utils.h"
#import "User.h"


@interface DashboardViewController () <DashboardProtocol>

@property (nonatomic, strong) DashboardPresenter *presenter;

@property (weak, nonatomic) IBOutlet UITableView *tbPosts;

@end

@implementation DashboardViewController

#pragma mark - Lifecyle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;

    [[Crashlytics sharedInstance] setGlobalUser];
}

#pragma mark - Presenters

- (void)didFetchPosts:(Post *)posts {

}

#pragma mark - IBActions

- (IBAction)didTapBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
