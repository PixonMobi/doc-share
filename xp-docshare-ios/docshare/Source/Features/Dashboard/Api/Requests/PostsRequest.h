//
//  PostsRequest.h
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "BaseRequest.h"

@interface PostsRequest : BaseRequest <NSCopying>

@property (nonatomic, strong) NSNumber *userId;

- (instancetype)initWithUser:(NSNumber *)userId;

@end
