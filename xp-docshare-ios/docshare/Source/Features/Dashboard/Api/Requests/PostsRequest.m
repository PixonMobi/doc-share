//
//  PostsRequest.m
//  Dynasty
//
//  Created by Danilo Pantani on 04/09/17.
//  Copyright © 2017 Danilo Pantani. All rights reserved.
//

#import "PostsRequest.h"

@implementation PostsRequest

#pragma mark - Init

- (instancetype)initWithUser:(NSNumber *)userId {
    self = [super init];
    if (self) {
        self.userId = userId;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    PostsRequest *copy = [[[self class] allocWithZone:zone] init];
    copy.userId = [self.userId copyWithZone:zone];
    return copy;
}

#pragma mark - Request

- (NSString *)endpoint {
    return kApiPosts;
}

- (NSString *)HTTPMethod {
    return @"POST";
}

- (NSDictionary *)params {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (self.userId) {
        [dic setValue:self.userId forKey:kPUserId];
    }
    
    return dic;
}

@end
