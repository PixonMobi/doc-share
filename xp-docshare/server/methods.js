Meteor.methods({
    resetData() {
        let testData = {
            'XPI': {
                'XPGE': {
                    'Risco Sacado': {
                        name: 'Risco Sacado',
                        date: '18/12/1987',
                        roles: [1, 2, 4],
                        file: 'http://teste',
                        type: 1,
                        active: true,
                    },
                    'Renda Fixa': {
                        name: 'Risco Sacado',
                        date: '18/12/1987',
                        roles: [1, 2, 4],
                        file: 'http://teste',
                        type: 1,
                        active: true,
                    },
                },
                'XP Associados': {
                    'Lançamentos': {
                        'Tutoriais': {
                            "Para Você": {
                                name: 'Risco Sacado',
                                date: '18/12/1987',
                                roles: [1, 2, 4],
                                file: 'http://teste',
                                type: 1,
                                active: true,
                            }
                        }
                    }
                }
            }
        };

        function insertTestData(parent, data) {
            for (let name in data) {
                let id = TreeData.insert({ name, parent });
                if (typeof data[name] === 'object') insertTestData(id, data[name]);
            }
        }

        TreeData.remove({});
        insertTestData(null, testData);
    }
})