import { Session } from 'meteor/session'
import { ReactiveVar } from 'meteor/reactive-var';

function recursiveCopy(id, parent) {
  let item = TreeData.findOne(id);
  delete item._id;
  item.parent = parent;
  let newId = TreeData.insert(item);

  TreeData.find({ parent: id }).forEach(function (item) {
    recursiveCopy(item._id, newId);
  });
}

function recursiveDelete(id) {
  TreeData.find({ parent: id }).forEach(function (item) {
    recursiveDelete(item._id);
  });
  TreeData.remove(id);
}

Template.Tree.onCreated(function () {
  var self = this;
  self.tableSource = new ReactiveVar();
  this.autorun(function () {
    var tableSource = getDataSource();
    self.tableSource.set(tableSource);
  });
});

Template.Tree.helpers({
  treeArgs() {
    let instance = Template.instance();
    let plugins = [];
    plugins.push('contextmenu');
    plugins.push('dnd');
    plugins.push('search');
    plugins.push('state');
    plugins.push('types');

    config = {
      collection: TreeData,
      subscription: 'TreeData',
      mapping: {
        text: 'name',
        aAttr: function (item) {
          return {
            title: item._id,
          };
        }
      },
      jstree: { plugins },
      events: {
        changed(e, item, data) {
          console.log('changed = ' + data);
        },
        create(e, item, data) {
          console.log('create = ' + data);
          return TreeData.insert({ name: 'New node', parent: data.parent });
        },
        rename(e, item, data) {
          console.log('rename = ' + data);
          TreeData.update(item, { $set: { name: data.text } });
        },
        delete(e, item, data) {
          console.log('delete = ' + data);
          recursiveDelete(item);
        },
        copy(e, item, data) {
          console.log('chacopynged = ' + data);
          if (data.parent == '#') {
            return false;
          }
          recursiveCopy(item, data.parent);
        },
        move(e, item, data) {
          console.log('move = ' + data);
          if (data.parent == '#') {
            return false;
          }
          TreeData.update(item, { $set: { parent: data.parent } });
        }
      }
    }
    return config;
  },
});


//================================================================================
// Getters
//================================================================================

function getDataSource() {
  return {
    Earth: {
      Europe: {
        Austria: {
          Vienna: null,
          Graz: null,
          Salzburg: null
        },
        Germany: {
          Berlin: null,
          Köln: null,
          Hamburg: null
        },
        Italy: {
          Roma: null,
          Venice: null,
          Trieste: null
        }
      },
      America: {
        "United States of America": {
          "New York": null,
          Philadelphia: null,
          "San Francisco": null
        }
      }
    }

  };
}