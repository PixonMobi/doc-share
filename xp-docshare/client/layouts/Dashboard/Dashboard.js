Template.Dashboard.onCreated(() => {

});

Template.Dashboard.helpers({

});

Template.ChartDelivered.helpers({
  chartDelivered() {
    return {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: "Entregues (30 de 45)"
      },
      tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            },
            connectorColor: 'silver'
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'genre',
        data: [
          ['Escritório 1', 45.0],
          ['Renda Fixa', 26.8],
          ['Renda Variável', 12.8],
          ['Geral', 8.5],
          ['XPGE', 6.2]
        ]
      }]
    };
  },
});

Template.ChartViewed.helpers({
  chartViewed() {
    return {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: "Vistos (18 de 30)"
      },
      tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            },
            connectorColor: 'silver'
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'genre',
        data: [
          ['Escritório 1', 25],
          ['Renda Fixa', 35],
          ['Renda Variável', 15],
          ['Geral', 15],
          ['XPGE', 10]
        ]
      }]
    };
  },
});

Template.DateChart.helpers({
  dateChart() {
    return {
      chart: {
        type: 'area'
      },
      title: {
        text: 'Visualização por Data'
      },
      credits: {
        enabled: false
      },
      subtitle: {
        text: 'Últimos 30 dias'
      },
      xAxis: {
        allowDecimals: false,
        labels: {
          formatter: function () {
            return this.value; // clean, unformatted number for year
          }
        }
      },
      yAxis: {
        title: {
          text: 'Visualizações'
        },
        labels: {
          formatter: function () {
            return this.value;
          }
        }
      },
      tooltip: {
        pointFormat: '<b>{point.y:,.0f}</b> viram {series.name}'
      },
      plotOptions: {
        area: {
          pointStart: 30,
          marker: {
            enabled: false,
            symbol: 'circle',
            radius: 2,
            states: {
              hover: {
                enabled: true
              }
            }
          }
        }
      },

      series: [{
        name: 'Risco Sacado',
        data: [null, null, null, null, null, 10, 40, 30, 50, 50, 30, 20, 30, 40, 43, 45, 234]
      }, {
        name: 'Conteúdo 2',
        data: [null, null, null, null, null, null, null, null, null, null,
          5, 25, 50, 43, 76, 34, 32, 4, 23, 54, 12, 45, 34, 45, 65, 87, 25, 76, 6, 23, 54, 76, 34, 12, 45, 76, 34]
      }]
    }
  }
});