import { Session } from 'meteor/session'
Template.Post.onCreated(function () {
  var self = this;
  self.uploading = new ReactiveVar(false);
  self.isLink = new ReactiveVar(false);

});

Template.Post.rendered=function() {
	$('#my-datepicker').datepicker();
}

Template.Post.helpers({
  uploading() {
    return Template.instance().uploading.get();
  },
  isLink() {
    return Template.instance().isLink.get();
  },
  treeArgs() {
    return config;
  },
});

Template.Post.events({
  'change [name="uploadCSV"]'(event, template) {
    template.uploading.set(true)
  },
  'click [name="isFileBox"]': function (event, template) {
    var newValue = event.target.checked;
    template.isLink.set(newValue);
  },
});
