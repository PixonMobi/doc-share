import { Session } from 'meteor/session'

Template.SideMenu.events({
    'click .btnMenuDash'(event, template) {
        Session.set('pageState', 1);
    },
    'click .btnMenuPosts'(event, template) {
        Session.set('pageState', 2);
    },
    'click .btnMenuPush'(event, template) {
        Session.set('pageState', 3);
    },
    'click .btnMenuLog'(event, template) {
        Session.set('pageState', 4);
    },
    'click .btnMenuDelivery'(event, template) {
        Session.set('pageState', 5);
    },
});

Template.SideMenu.helpers({
    menuItems: [
        {
            class: 'btnMenuDash', name: 'Dashboard',
            active: function () {
                return Session.get('pageState') === 1
            }
        },
        {
            class: 'btnMenuPosts', name: 'Conteúdos',
            active: function () {
                return Session.get('pageState') === 2
            }
        },
        {
            class: 'btnMenuPush', name: 'Notificações',
            active: function () {
                return Session.get('pageState') === 3
            }
        },
        {
            class: 'btnMenuLog', name: 'Auditoria',
            active: function () {
                return Session.get('pageState') === 4
            }
        },
        {
            class: 'btnMenuDelivery', name: 'Entregas',
            active: function () {
                return Session.get('pageState') === 5
            }
        },
    ],
});