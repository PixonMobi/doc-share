import { Session } from 'meteor/session'

Template.Content.onCreated(() => {
    Session.set('pageState', 1);
});

Template.Content.helpers({
    pageState() {
        return Session.get('pageState');
    },
    equals: function (v1, v2) {
        return v1 === v2
    },
});