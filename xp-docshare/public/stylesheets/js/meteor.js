$(document).ready(function () {

    // Toggle Search
    $('.show-search').click(function () {
        $('.search-form').css('margin-top', '0');
        $('.search-input').focus();
    });

    $('.close-search').click(function () {
        $('.search-form').css('margin-top', '-70px');
    });

    // Fullscreen
    function toggleFullScreen() {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {
                document.documentElement.requestFullScreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullScreen) {
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }

    // tooltips
    $('[data-toggle~="tooltip"]').tooltip({
        container: 'body'
    });

    // Element Blocking
    function blockUI(item) {
        $(item).block({
            message: '<img src="assets/images/reload.gif" width="20px" alt="">',
            css: {
                border: 'none',
                padding: '0px',
                width: '20px',
                height: '20px',
                backgroundColor: 'transparent'
            },
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.9,
                cursor: 'wait'
            }
        });
    }

    function unblockUI(item) {
        $(item).unblock();
    }

    // Panel Control
    $('.panel-collapse').click(function () {
        $(this).closest(".panel").children('.panel-body').slideToggle('fast');
    });

    $('.panel-reload').click(function () {
        var el = $(this).closest(".panel").children('.panel-body');
        blockUI(el);
        window.setTimeout(function () {
            unblockUI(el);
        }, 1000);

    });

    $('.panel-remove').click(function () {
        $(this).closest(".panel").hide();
    });

    // Push Menu
    $('.push-sidebar').click(function () {
        var hidden = $('.sidebar');

        if (hidden.hasClass('visible')) {
            hidden.removeClass('visible');
            $('.page-inner').removeClass('sidebar-visible');
        } else {
            hidden.addClass('visible');
            $('.page-inner').addClass('sidebar-visible');
        }

        $('.push-sidebar i').toggleClass("icon-arrow-left");
        $('.push-sidebar i').toggleClass("icon-arrow-right");
    });

    // .toggleAttr() Function
    $.fn.toggleAttr = function (a, b) {
        var c = (b === undefined);
        return this.each(function () {
            if ((c && !$(this).is("[" + a + "]")) || (!c && b)) $(this).attr(a, a);
            else $(this).removeAttr(a);
        });
    };

    // Sidebar Menu
    var parent, ink, d, x, y;
    $('.sidebar .accordion-menu li .sub-menu').slideUp(0);
    $('.sidebar .accordion-menu li.open .sub-menu').slideDown(0);
    $('.small-sidebar .sidebar .accordion-menu li.open .sub-menu').hide(0);
    $('.sidebar .accordion-menu > li.droplink > a').click(function () {

        if (!($('body').hasClass('small-sidebar')) && (!$('body').hasClass('page-horizontal-bar')) && (!$('body').hasClass('hover-menu'))) {

            var menu = $('.sidebar .menu'),
                sidebar = $('.page-sidebar-inner'),
                page = $('.page-content'),
                sub = $(this).next(),
                el = $(this);

            menu.find('li').removeClass('open');
            $('.sub-menu').slideUp(200, function () {
                sidebarAndContentHeight();
            });
            sidebarAndContentHeight();

            if (!sub.is(':visible')) {
                $(this).parent('li').addClass('open');
                $(this).next('.sub-menu').slideDown(200, function () {
                    sidebarAndContentHeight();
                });
            } else {
                sub.slideUp(200, function () {
                    sidebarAndContentHeight();
                });
            }
            return false;
        };

        if (($('body').hasClass('small-sidebar')) && ($('body').hasClass('page-sidebar-fixed'))) {

            var menu = $('.sidebar .menu'),
                sidebar = $('.page-sidebar-inner'),
                page = $('.page-content'),
                sub = $(this).next(),
                el = $(this);

            menu.find('li').removeClass('open');
            $('.sub-menu').slideUp(200, function () {
                sidebarAndContentHeight();
            });
            sidebarAndContentHeight();

            if (!sub.is(':visible')) {
                $(this).parent('li').addClass('open');
                $(this).next('.sub-menu').slideDown(200, function () {
                    sidebarAndContentHeight();
                });
            } else {
                sub.slideUp(200, function () {
                    sidebarAndContentHeight();
                });
            }
            return false;
        };
    });

    $('.sidebar .accordion-menu .sub-menu li.droplink > a').click(function () {

        var menu = $(this).parent().parent(),
            sidebar = $('.page-sidebar-inner'),
            page = $('.page-content'),
            sub = $(this).next(),
            el = $(this);

        menu.find('li').removeClass('open');
        sidebarAndContentHeight();

        if (!sub.is(':visible')) {
            $(this).parent('li').addClass('open');
            $(this).next('.sub-menu').slideDown(200, function () {
                sidebarAndContentHeight();
            });
        } else {
            sub.slideUp(200, function () {
                sidebarAndContentHeight();
            });
        }
        return false;
    });

    // Small Fixed Sidebar
    var onSidebarHover = function () {
        $('.small-sidebar.page-sidebar-fixed:not(.page-horizontal-bar) .page-sidebar').mouseenter(function () {
            $('body:not(.page-horizontal-bar) .navbar').addClass('hovered-sidebar');
            $('.small-sidebar.page-sidebar-fixed:not(.page-horizontal-bar) .navbar .logo-box a span').html($('.navbar .logo-box a span').text() == smTxt ? str : smTxt);
        });
        $('.small-sidebar.page-sidebar-fixed:not(.page-horizontal-bar) .page-sidebar').mouseleave(function () {
            $('body:not(.page-horizontal-bar) .navbar').removeClass('hovered-sidebar');
            $('.small-sidebar.page-sidebar-fixed:not(.page-horizontal-bar) .navbar .logo-box a span').html($('.navbar .logo-box a span').text() == smTxt ? str : smTxt);
        });
    };

    onSidebarHover();

    // Makes .page-inner height same as .page-sidebar height
    var sidebarAndContentHeight = function () {
        var content = $('.page-inner'),
            sidebar = $('.page-sidebar'),
            body = $('body'),
            height,
            footerHeight = $('.page-footer').outerHeight(),
            pageContentHeight = $('.page-content').height();

        content.attr('style', 'min-height:' + sidebar.height() + 'px !important');
        if (body.hasClass('page-sidebar-fixed')) {
            height = sidebar.height() + footerHeight;
        } else {
            height = sidebar.height() + footerHeight;
            if (height < $(window).height()) {
                height = $(window).height();
            }
        };

        if (height >= content.height()) {
            content.attr('style', 'min-height:' + height + 'px !important');
        };
        if (body.hasClass('page-horizontal-bar')) {
            content.attr('style', 'min-height:' + $(window).height() + 'px !important');
        };
        if ((!sidebar.length) && (!$('.navbar').length)) {
            content.attr('style', 'min-height:' + pageContentHeight + 'px !important');
        }
    };

    sidebarAndContentHeight();

    window.onresize = sidebarAndContentHeight;

    // Layout Settings
    var fixedHeaderCheck = document.querySelector('.fixed-header-check'),
        fixedSidebarCheck = document.querySelector('.fixed-sidebar-check'),
        horizontalBarCheck = document.querySelector('.horizontal-bar-check'),
        toggleSidebarCheck = document.querySelector('.toggle-sidebar-check'),
        boxedLayoutCheck = document.querySelector('.boxed-layout-check'),
        compactMenuCheck = document.querySelector('.compact-menu-check'),
        hoverMenuCheck = document.querySelector('.hover-menu-check'),
        defaultOptions = function () {

            if (($('body').hasClass('small-sidebar')) && (toggleSidebarCheck.checked == 1)) {
                toggleSidebarCheck.click();
            }

            if (($('body').hasClass('page-header-fixed')) && (fixedHeaderCheck.checked == 1)) {
                fixedHeaderCheck.click();
            }

            if (($('body').hasClass('page-sidebar-fixed')) && (fixedSidebarCheck.checked == 1)) {
                fixedSidebarCheck.click();
            }

            if (($('body').hasClass('page-horizontal-bar')) && (horizontalBarCheck.checked == 1)) {
                horizontalBarCheck.click();
            }

            if (!($('body').hasClass('compact-menu')) && (compactMenuCheck.checked == 0)) {
                compactMenuCheck.click();
            }

            if (($('body').hasClass('hover-menu')) && (hoverMenuCheck.checked == 1)) {
                hoverMenuCheck.click();
            }

            if (($('.page-content').hasClass('container')) && (boxedLayoutCheck.checked == 1)) {
                boxedLayoutCheck.click();
            }

            sidebarAndContentHeight();
        },
        str = $('.navbar .logo-box a span').text(),
        smTxt = (str.slice(0, 1)),
        collapseSidebar = function () {
            $('body').toggleClass("small-sidebar");
            $('.navbar .logo-box a span').html($('.navbar .logo-box a span').text() == smTxt ? str : smTxt);
            sidebarAndContentHeight();
            $('.sidebar-toggle i').toggleClass("icon-arrow-left");
            $('.sidebar-toggle i').toggleClass("icon-arrow-right");
            onSidebarHover();
        },
        fixedHeader = function () {
            if (($('body').hasClass('page-horizontal-bar')) && ($('body').hasClass('page-sidebar-fixed')) && ($('body').hasClass('page-header-fixed'))) {
                fixedSidebarCheck.click();
                alert("Static header isn't compatible with a fixed horizontal nav mode. I will set a static mode on the horizontal nav.");
                onSidebarHover();
            };
            $('body').toggleClass('page-header-fixed');
            sidebarAndContentHeight();
            onSidebarHover();
        },
        fixedSidebar = function () {
            if (($('body').hasClass('page-horizontal-bar')) && (!$('body').hasClass('page-sidebar-fixed')) && (!$('body').hasClass('page-header-fixed'))) {
                fixedHeaderCheck.click();
                alert("Fixed horizontal nav isn't compatible with a static header mode. I will set a fixed mode on the header.");
            };
            if (($('body').hasClass('hover-menu')) && (!$('body').hasClass('page-sidebar-fixed'))) {
                hoverMenuCheck.click();
                alert("Fixed sidebar isn't compatible with a hover menu mode. I will set an accordion mode on the menu.");
            };
            $('body').toggleClass('page-sidebar-fixed');
            if ($('body').hasClass('.page-sidebar-fixed')) {
                $('.page-sidebar-inner').slimScroll({
                    destroy: true
                });
            };
            $('.page-sidebar-inner').slimScroll();
            sidebarAndContentHeight();
            onSidebarHover();
        },
        horizontalBar = function () {
            $('.sidebar').toggleClass('horizontal-bar');
            $('.sidebar').toggleClass('page-sidebar');
            $('body').toggleClass('page-horizontal-bar');
            if (($('body').hasClass('page-sidebar-fixed')) && (!$('body').hasClass('page-header-fixed'))) {
                fixedHeaderCheck.click();
                alert("Static header isn't compatible with a fixed horizontal nav mode. I will set a static mode on the horizontal nav.");
            };
            sidebarAndContentHeight();
            onSidebarHover();
        },
        boxedLayout = function () {
            $('.page-content').toggleClass('container');
            $('.search-form').toggleClass('boxed-layout-search');
            $('.search-form .input-group').toggleClass('container');
            sidebarAndContentHeight();
            onSidebarHover();
        },
        compactMenu = function () {
            $('body').toggleClass('compact-menu');
            sidebarAndContentHeight();
            onSidebarHover();
        },
        hoverMenu = function () {
            if ((!$('body').hasClass('hover-menu')) && ($('body').hasClass('page-sidebar-fixed'))) {
                fixedSidebarCheck.click();
                alert("Fixed sidebar isn't compatible with a hover menu mode. I will set a static mode on the sidebar.");
                onSidebarHover();
            };
            $('body').toggleClass('hover-menu');
            sidebarAndContentHeight();
            onSidebarHover();
        };

    // Logo text on Collapsed Sidebar
    $('.small-sidebar .navbar .logo-box a span').html($('.navbar .logo-box a span').text() == smTxt ? str : smTxt);
});